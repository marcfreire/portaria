<?php

require_once __DIR__.'/../myconfig.php';



class CidadeDAO {

    public $pdo;

    public function __construct() {

       $this->pdo = ConnectFactory::getConnect();
    }


    public function pesquisaPorEstado($idEstado) {

        try {

            $stmt = $this->pdo->prepare("select * from cidade where idestado= :idestado order by cidade.nome ");

            $stmt->bindParam(':idestado', $idEstado, PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }


}


