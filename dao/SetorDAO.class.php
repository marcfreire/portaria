<?php

require_once __DIR__ . '/../myconfig.php';

class SetorDAO {

    public $pdo;

    public function __construct() {

        $this->pdo = ConnectFactory::getConnect();
    }

    public function lista() {

        try {

            $stmt = $this->pdo->prepare("
                select s.* from setor as s    where s.stdelete = 1
           ");

           

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);

          

            return $result;
            
            
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

}
