<?php

require_once __DIR__ . '/../myconfig.php';

class InscricaoDAO {

    public $pdo;

    public function __construct() {

        $this->pdo = ConnectFactory::getConnect();
    }

    public function load($id) {

        try {

            $stmt = $this->pdo->prepare("
                
                                        select i.*,e.nome as estado_desc,c.nome as cidade_desc
                                        from inscricao i 
                                        left join estado as e on e.idestado = i.idestado
                                        left join cidade as c on c.idcidade = i.idcidade
                                       

                                       
                                        
                                        
                                        
                                        
                                        where idinscricao =:id and i.stdelete=1");

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_OBJ);

            $inscricao = DAO::fetch('Inscricao', $result);

            if ($result) {

                
                $inscricao->estado->idestado = $result->idestado;
                $inscricao->cidade->idcidade = $result->idcidade;
                
                
            }


            return $inscricao;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }
        
        return false;
    }
    
    
    public function loadPorCPF($cpf) {

        try {

              $stmt = $this->pdo->prepare("
                  
                                        select i.*,e.nome as estado_desc,c.nome as cidade_desc
                                        from inscricao i 
                                        left join estado as e on e.idestado = i.idestado
                                        left join cidade as c on c.idcidade = i.idcidade
                                        
                                      
                                        
                                        where  i.stdelete=1 and  tira_tracos_pontos_barra(cpf) =tira_tracos_pontos_barra(:cpf)");

            $stmt->bindValue(':cpf', $cpf, PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_OBJ);
            
           

            $inscricao = DAO::fetch('Inscricao', $result);

           
            
            if ($result) {

                
                $inscricao->estado->idestado = $result->idestado;
                $inscricao->cidade->idcidade = $result->idcidade;
                
                
            }
          


            return $inscricao;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

   

    public function pesquisa($inscricao) {



        try {

            $stmt = $this->pdo->prepare("select i.*
                                        
                                        from inscricao i 
                                       
                                        where i.stdelete=1 and status_cadastro=2 order by i.nome");


            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);


            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

    public function verifica_cpf_email($cpf, $email) {


        try {

            $stmt = $this->pdo->prepare("select * from inscricao  where email = :email or tira_tracos_pontos_barra(cpf) = tira_tracos_pontos_barra(:cpf)");



            $stmt->bindValue(':cpf', $cpf, PDO::PARAM_STR);
            $stmt->bindValue(':email', $email, PDO::PARAM_STR);



            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_OBJ);


            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

    // SESSION

    public static function saveUserSession($inscricao) {


        $_SESSION['usuario_inscrito_tr1'] = $inscricao;
    }

    public static function logout() {

        unset($_SESSION['usuario_inscrito_tr1']);

        
    }

    public static function loadUserSession($clone = true) {

        $inscricao = new Inscricao();

        if (isset($_SESSION['usuario_inscrito_tr1'])) {
            $inscricao = $_SESSION["usuario_inscrito_tr1"];
        }
        if ($clone) {

            return clone $inscricao;
        } else {
            return $inscricao;
        }
    }

    public static function isUserLogged() {

        if (isset($_SESSION['usuario_inscrito_tr1'])) {

            return true;
        }
        return false;
    }

}
