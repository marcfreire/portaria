<?php

require_once __DIR__.'/../myconfig.php';


class MetadadosDAO {

    public $pdo;

 

    public function __construct() {


        $this->pdo = ConnectFactory::getConnect();
    }
    
    public function listaPorCodigo($cod) {

        try {

            $stmt = $this->pdo->prepare("select * from metadados as m where m.codigo = :codigo and stdelete=1 order by descricao");

            $stmt->bindParam(':codigo', $cod, PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }
    
     public function load($id) {

        try {

            $stmt = $this->pdo->prepare("select * from metadados as m where m.idmetadados =:id");

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_OBJ);
            
            $metadados= DAO::fetch('Metadados', $result);

         


            return $metadados;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

   
}
