<?php

require_once __DIR__ . '/../myconfig.php';

/**
 * Description of UsuarioDAO
 *
 * @author Marcos
 */
class UnidadeDAO {

    public $pdo;

    public function __construct() {


        $this->pdo = ConnectFactory::getConnect();
    }

    

    public function lista() {

        try {

            
           

            $stmt = $this->pdo->prepare("
                   select * from unidade where stdelete=1 and idunidade >=100
                    ");
            
            

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);



            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }


}
