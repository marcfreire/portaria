<?php

require_once __DIR__ . '/../myconfig.php';

class VisitaDAO {

    public $pdo;

    public function __construct() {

        $this->pdo = ConnectFactory::getConnect();
    }

    public function load($id) {



        try {

            $stmt = $this->pdo->prepare("
              select v.*,s.nome as setor_nome,i.nome as visita_nome,m.nome as setor_mestre_nome
              from visita as v 
              left join setor as s on v.idsetor = s.idsetor  
              left join setor as m on m.idsetor = s.setormestre  
              join inscricao as i on i.idinscricao = v.idinscricao 
              where v.stdelete=1  and v.idvisita = :idvisita
           ");


            $stmt->bindValue(':idvisita', $id, PDO::PARAM_INT);


            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_OBJ);



            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

    public function listaDia()
    {
        $sqlInscricao = '';

        try {

            $stmt = $this->pdo->prepare("
               select v.*,s.nome as setor_nome,i.nome as visita_nome,m.nome as setor_mestre_nome
               from visita as v 
               left join setor as s on v.idsetor = s.idsetor 
               left join setor as m on m.idsetor = s.setormestre                
               inner join inscricao as i on i.idinscricao = v.idinscricao  where v.stdelete=1 and v.datacadastro::date = now()::date 
           ");

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;   
    }

    public function lista($idinscricao = false) {


        $sqlInscricao = '';

        if ($idinscricao) {
            $sqlInscricao = '  and i.idinscricao = :idinscricao ';
        }

        try {

            $stmt = $this->pdo->prepare("
               select v.*,s.nome as setor_nome,i.nome as visita_nome,m.nome as setor_mestre_nome
               from visita as v 
               left join setor as s on v.idsetor = s.idsetor 
               left join setor as m on m.idsetor = s.setormestre                
               inner join inscricao as i on i.idinscricao = v.idinscricao  where v.stdelete=1 $sqlInscricao
           ");

            if ($idinscricao) {
                $stmt->bindValue(':idinscricao', $idinscricao, PDO::PARAM_INT);
            }

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);



            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

    public function listaPorSetor($idsetor) {




        try {

            $stmt = $this->pdo->prepare("
               select v.*,s.nome as setor_nome,i.nome as visita_nome,m.nome as setor_mestre_nome
               from visita as v 
               left join setor as s on v.idsetor = s.idsetor 
               left join setor as m on m.idsetor = s.setormestre                
               inner join inscricao as i on i.idinscricao = v.idinscricao  where v.stdelete=1 and v.idsetor = :idsetor
           ");


            $stmt->bindValue(':idsetor', $idsetor, PDO::PARAM_INT);

           

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);



            return $result;
        } catch (PDOException $i) {

            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

    public function listaPorSetorPendente($idsetor) {




        try {

            $this->pdo->beginTransaction();

            $stmt = $this->pdo->prepare("
               select v.*,s.nome as setor_nome,i.nome as visita_nome,m.nome as setor_mestre_nome,i.foto
               from visita as v 
               left join setor as s on v.idsetor = s.idsetor 
               left join setor as m on m.idsetor = s.setormestre                
               inner join inscricao as i on i.idinscricao = v.idinscricao  where v.stdelete=1 and v.idsetor = :idsetor
               and (notificada is null or notificada=0)
              
           ");


            $stmt->bindValue(':idsetor', $idsetor, PDO::PARAM_INT);



            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);

            $retorno = [];
            foreach($result as $r){
                $r->foto = ($r->foto);
                $retorno[] = $r;
            }
          

            $this->pdo->commit();

            return $retorno;
        } catch (PDOException $i) {

            $this->pdo->rollBack();
            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }
    
     public function finalizar($idsetor) {




        try {

            $this->pdo->beginTransaction();

          

            $stmt = $this->pdo->prepare("
               Update visita set notificada = 1   where stdelete=1 and idsetor = :idsetor and (notificada is null or notificada=0)
              
           ");

            $stmt->bindValue(':idsetor', $idsetor, PDO::PARAM_INT);
            $stmt->execute();

            $this->pdo->commit();

            return true;
        } catch (PDOException $i) {

            $this->pdo->rollBack();
            echo "Erro: <code>" . $i->getMessage() . "</code>";
        }

        return false;
    }

    public function loadRelatorioVisitas($dataInicio, $dataFim, $sala)
    {
        try {

            $sql = "SELECT
                i.nome as nome_visitante,
                v.datacadastro,
                v.checkout,
                s.nome as nome_setor
            FROM
                visita v
                LEFT JOIN setor s ON v.idsetor = s.idsetor
                LEFT JOIN setor as m on m.idsetor = s.setormestre
                INNER JOIN inscricao i ON i.idinscricao = v.idinscricao
            WHERE
                v.stdelete = 1 AND
                v.datacadastro::date BETWEEN :dataInicio AND :dataFim
            ";

            if ($sala) {
                $sql .= " AND v.idsetor = :sala";
            }

            $stmt = $this
                ->pdo
                ->prepare($sql);

            $stmt->bindValue(':dataInicio', $dataInicio);
            $stmt->bindValue(':dataFim', $dataFim);
            
            if ($sala) {
                $stmt->bindValue(':sala', $sala);
            }


            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_OBJ);


            return $result;
        } catch (PDOException $i) {
            echo "Erro: <code>" . $i->getMessage() . "</code>";
            return false;
        }
    }

}
