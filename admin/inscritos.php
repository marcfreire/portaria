<?
require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/admin.inc.php';

Controller_defaultMethod('pesquisa');

require_once (_file('/controllers/InscricaoCtrl.php'));
?>

<!doctype html>
<html lang="pt-br">

    <head>

<? require_once _file("/admin/templates/part_head.inc.php"); ?>

    </head>

    <body>
        <div class="wrapper">


<? require_once _file("/admin/templates/part_sidemenu.inc.php"); ?>


            <div class="main-panel">

<? require_once _file("/admin/templates/part_nav.inc.php"); ?>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">




                                    <div class="content">

                                        <h4 class="title"> Inscritos </h4>
                                        <div class="toolbar">
                                            <!--        Here you can write extra buttons/actions for the toolbar   
    
                                            -->

<? if (isset($listaInscricao[0])): ?>
                                                <? if (count($listaInscricao) == 0): ?>
                                                    <div class="alert alert-info text-center" style="color: black; font-weight: 700; font-size: 20px;" >Nenhum inscrito até o momento</div>

    <? else: ?>

                                                    <div class="alert alert-info text-center" style="color: black; font-weight: 700; font-size: 20px;" > <?= count($listaInscricao) ?> inscrito(s) </div>

    <? endif ?>
                                            <? endif ?>






                                        </div>
                                        <div class="material-datatables">
                                            <table id="datatables" class="table table-striped table-no-bordered table-hover" style="width:100%;cellspacing:0">
                                                <thead>
                                                    <tr>

                                                        <th>Nome</th>
                                                        <th>CPF</th>
                                                        <th>Email</th>
                                                        <th>Status</th>


                                                        <th class="disabled-sorting ">Ações</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
<? if (isset($listaInscricao[0])): ?>
                                                        <? foreach ($listaInscricao as $item): ?>
                                                            <tr>

                                                                <td><?= $item->nome ?></td>
                                                                <td><?= $item->cpf ?></td>
                                                                <td><?= $item->email ?></td>
                                                               <td><?= ModeloCombo::$boxStatusInscricao[$item->status]?></td>

                                                                <td>


                                                                    <div class="btn-toolbar" role="toolbar">
                                                                        <div class="btn-group btn-group-seperator">

                                                                            <a type="button" href="<?= _src('/admin/detalhes.php?id=') . $item->idinscricao ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalhes do inscrito"><i class="fa fa-eye " aria-hidden="true"></i> Detalhes</a>





                                                                            </tr>
    <? endforeach ?>
                                                                    <? endif ?>

                                                                    </tbody>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <!-- end content-->
                                                            </div>
                                                            <!--  end card  -->
                                                            </div>
                                                            <!-- end col-md-12 -->
                                                            </div>



                                                            </div>
                                                            </div>


<? require_once _file("/admin/templates/part_footer.inc.php"); ?>


                                                            </div>

                                                            </div>
<? require_once _file("/admin/templates/part_js.inc.php"); ?>

                                                            <script type="text/javascript">
                                                                $(document).ready(function () {

                                                                    $('.popover-hover').popover({trigger: "hover", html: true});

                                                                    $('#datatables').DataTable({
                                                                        "pagingType": "full_numbers",
                                                                        "lengthMenu": [
                                                                            [10, 25, 50, -1],
                                                                            [10, 25, 50, "All"]
                                                                        ],
                                                                        responsive: true,
                                                                        language: {
                                                                            //search: "",
                                                                            //searchPlaceholder: "",
                                                                        }

                                                                    });


                                                                    var table = $('#datatables').DataTable();

                                                                    // Edit record
                                                                    table.on('click', '.edit', function () {
                                                                        $tr = $(this).closest('tr');

                                                                        var data = table.row($tr).data();
                                                                        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
                                                                    });

                                                                    // Delete a record
                                                                    table.on('click', '.remove', function (e) {
                                                                        $tr = $(this).closest('tr');
                                                                        table.row($tr).remove().draw();
                                                                        e.preventDefault();
                                                                    });

                                                                    //Like record
                                                                    table.on('click', '.like', function () {
                                                                        alert('You clicked on Like button');
                                                                    });

                                                                    $('.card .material-datatables label').addClass('form-group');


                                                                });
                                                            </script>
                                                            </body>
                                                            </html>
