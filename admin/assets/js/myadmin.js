(function($) {
    "use strict";

    $(document).ready(function() {

       

		$.extend($.fn.dataTable.defaults, {
		    "iDisplayLength": 50,
		    "language": {
		         "sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_ resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": "Próximo",
			        "sPrevious": "Anterior",
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
		    }
		});





        alertify.defaults = {

            autoReset: true,
            basic: false,
            closable: true,
            closableByDimmer: true,
            frameless: false,
            maintainFocus: true,
            maximizable: true,
            modal: true,
            movable: true,
            moveBounded: false,
            overflow: true,
            padding: true,
            pinnable: true,
            pinned: true,
            preventBodyShift: false,
            resizable: true,
            startMaximized: false,
            transition: 'pulse',
            defaultFocus: 'cancel',

            notifier: {

                delay: 5,

                position: 'top-right',

                closeButton: false
            },

            glossary: {

                title: 'Negro Cosme',

                ok: 'OK',

                cancel: 'Cancelar'
            },

            theme: {
                // class name attached to prompt dialog input textbox.
                input: 'ajs-input',
                // class name attached to ok button
                ok: 'ajs-ok',
                // class name attached to cancel button 
                cancel: 'ajs-cancel'
            }
        };

        $.validator.setDefaults({
            ignoreTitle: true
        });

        $.validator.addMethod("tel_minlength", $.validator.methods.minlength, "Formato de telefone inválido");

        $.validator.addMethod(
                "time",
                function (value, element) {
                    var check = false;
                    var re = /^\d{2,}:\d{2}$/;
                    if (re.test(value)) {
                        var atime = value.split(':');
                        var hh = parseInt(atime[0], 10);
                        var mm = parseInt(atime[1], 10);
                        if (hh >= 0 && hh <= 23 && mm >= 0 && mm <= 59) {
                            check = true;
                        } else {
                            check = false;
                        }
                    } else
                        check = false;
                    return this.optional(element) || check;
                },
                "Formato de hora inválida"
                );

        $.validator.addMethod(
                "dateBR",
                function (value, element) {
                    var check = false;
                    var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                    if (re.test(value)) {
                        var adata = value.split('/');
                        var gg = parseInt(adata[0], 10);
                        var mm = parseInt(adata[1], 10);
                        var aaaa = parseInt(adata[2], 10);
                        var xdata = new Date(aaaa, mm - 1, gg);
                        if ((xdata.getFullYear() == aaaa) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == gg))
                            check = true;
                        else
                            check = false;
                    } else
                        check = false;
                    return this.optional(element) || check;
                },
                "Formato de data inválida"
                );

        $.validator.addMethod(
                "dayOfMonth",
                function (value, element) {
                    var check = value > 0 && value <= 31;
                    return this.optional(element) || check;
                },
                "Dia inválido"
                );
        $.validator.addMethod(
                "cpf",
                function (value, element) {
                    var check = validaCPF(value);
                    return this.optional(element) || check;
                },
                "CPF formato inválido"
                );

        $.validator.addMethod(
                "cnpj",
                function (value, element) {
                    var check = validaCnpj(value);
                    return this.optional(element) || check;
                },
                "CNPJ formato inválido"
                );

        $.validator.addMethod(
                "nota",
                function (value, element) {
                    var v = value.replace(/,/, ".");
                    var check = $.isNumeric(v) && v >= 0 && v <= 10;

                    return this.optional(element) || check;
                },
                "Nota inválida"
                );
        $.validator.addMethod(
                "double",
                function (value, element) {
                    var v = value;
                    var check = $.isNumeric(v) && v >= 0;

                    return this.optional(element) || check;
                },
                "Valor inválido"
        );

           $.extend(jQuery.validator.messages, {
              
               
                max: jQuery.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
                min: jQuery.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
            });

        $.validator.addClassRules({
            v_req: {
                required: true

            },
            v_min3: {
                minlength: 3
            },
            v_min6: {
                minlength: 6
            },
            v_email: {
                email: true
            },
            v_tel: {
                tel_minlength: 13

            },
            v_date: {
                dateBR: true

            },
            v_time: {
                time: true,
                minlength: 5

            },
            v_day: {
                dayOfMonth: true

            },
            v_cpf: {
                cpf: 'both'

            },
            v_cnpj: {
                cnpj: 'both'

            },
            v_nota: {
                nota: 'both'

            },
             v_double: {
                double: 'both'


            }


        });
        
 

      

        $("form:not(.val_not)").validate({
            ignore: "",
            invalidHandler: function (form, validator) {
                //$(window).scrollTop(0);
            invalidShow();
                $('.nav-tabs li a').removeClass('error');
                for (var i = 0; i < validator.errorList.length; i++) {
                    var e = validator.errorList[i].element;
                    var idTab = $(e).closest('.tab-pane').attr('id');
                    $("li a[href='#" + idTab + "']").addClass('error');

                }


            }
        });

        $('.masknumber').mask('999999999');
        $('.masktime').mask('99:99');
        $(".maskdate").mask("99/99/9999");
        $(".masktel").mask("(00) 000000000");
        $(".maskcpf").mask("000.000.000-00");
        $(".maskcnpj").mask("00.000.000/0000-00");
        $('.maskmoney').mask('000.000.000.000.000,00', {reverse: true});
        $('.maskcep').mask('00000-000');
        $('.maskrg').mask('000000000000-0');
        $('.masknota').mask('99,9');
        




   });
}(jQuery));

function invalidShow() {

    alertify.notify('Alguns campos não foram prenchidos corretamente', 'error', 5);
}

function notify(msg, cor) {

     alertify.notify(msg, cor , 5);
     
}
