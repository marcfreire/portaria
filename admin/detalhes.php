<?
require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/admin.inc.php';

Controller_defaultMethod('detalhes');

require_once (_file('/controllers/InscricaoCtrl.php'));
?>

<!doctype html>
<html lang="pt-br">

    <head>

        <? require_once _file("/admin/templates/part_head.inc.php"); ?>

        <style>


        </style>

    </head>

    <body>
        <div class="wrapper">


            <? require_once _file("/admin/templates/part_sidemenu.inc.php"); ?>


            <div class="main-panel">

                <? require_once _file("/admin/templates/part_nav.inc.php"); ?>


                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card" id="profile-main">

                                    <div class="content">
                                        <div class="">

                                            <a href="<?= _src('/admin/inscritos.php') ?>" class="btn btn-default m-b-10">
                                                <span class="btn-label">
                                                    <i class="ti-search"></i>
                                                </span>
                                                Pesquisar
                                            </a>




                                           

                                                <button id="classificar"  type="button" class="btn btn-success m-b-10 classificar"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Classificar</button>

                                                <button  id="desclassificar" type="button" class="btn btn-danger m-b-10"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Desclassificar</button>



                                            <button id="excluir" class="btn btn-danger m-b-10"><i class="fa fa-trash" aria-hidden="true"></i> Excluir</button>
                                            <!--
                                            <a target="_blank" href="<?= _src('/segunda_via_comprovante_inscricao.pdf?id=') . $inscrito->idinscricao ?>" class="btn btn-info m-b-10">
                                                <span class="btn-label">
                                                    <i class="fa fa-print" aria-hidden="true"></i>
                                                </span>
                                                Imprimir Comprovante
                                            </a>
                                            -->
                                        </div>
                                    </div>



                                    <? if ($inscrito->status == Inscricao::STATUS_PENDENTE): ?>

                                        <div class="content" style="margin-bottom: -30px;">
                                            <div class="alert alert-info text-center" style="color: white; font-weight: 700; font-size: 20px;" >
                                                <?= ModeloCombo::$boxStatusInscricao[$inscrito->status] ?>
                                            </div>
                                        </div>
                                    <? elseif ($inscrito->status == Inscricao::STATUS_CLASSIFICADO): ?>

                                        <div class="content"  style="margin-bottom: -30px;">
                                            <div class="alert alert-success text-center" style="color: white; font-weight: 700; font-size: 20px;" >
                                                <?= ModeloCombo::$boxStatusInscricao[$inscrito->status] ?>
                                            </div>
                                        </div>

                                    <? elseif ($inscrito->status == Inscricao::STATUS_DESCLASSIFICADO): ?>

                                        <div class="content"  style="margin-bottom: -30px;">
                                            <div class="alert alert-danger text-center" style="color: white; font-weight: 700; font-size: 20px;" >
                                                <?= ModeloCombo::$boxStatusInscricao[$inscrito->status] ?>
                                            </div>
                                        </div>

                                    <? endif ?>



                                    <div class="header">
                                        <legend>
                                            Detalhes do Inscrito

                                            <? if (!empty($inscricao->idinscricao)): ?>   
                                                <button type="button" data-id="<?= $inscricao->idinscricao ?>" class="btn btn-danger btn-fill excluir-inscricao" style="float: right; margin-top: -12px; margin-right: 10px;">
                                                    <span class="btn-label">
                                                        <i class="ti-trash"></i>
                                                    </span>
                                                    Excluir
                                                </button>
                                            <? endif ?>
                                        </legend>
                                    </div>
                                    <div class="content">


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="p-15">

                                                    <div class="row m-l-5">

                                                        <? if (!empty($inscrito->idinscricao)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Número de Inscrição</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->idinscricao ?></p>
                                                            </div>
                                                        <? endif ?>


                                                        <? if (!empty($inscrito->nome)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Nome</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->nome ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->sexo)): ?>

                                                            <div class="col-sm-4">
                                                                <p><strong>Sexo</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= ModeloCombo::$boxSexo[$inscrito->sexo] ?></p>
                                                            </div>

                                                        <? endif ?>

                                                        <? if (!empty($inscrito->rg)): ?>

                                                            <div class="col-sm-4">
                                                                <p><strong>RG</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->rg ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->nis)): ?>

                                                            <div class="col-sm-4">
                                                                <p><strong>NIS (CAD. ÚNICO)</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->nis ?></p>
                                                            </div>

                                                        <? endif ?>

                                                        <? if (!empty($inscrito->cpf)): ?>

                                                            <div class="col-sm-4">
                                                                <p><strong>CPF</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->cpf ?></p>
                                                            </div>

                                                        <? endif ?>
                                                        
                                                        <div class="col-sm-4">
                                                                <p><strong>Telefones</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->tel1?> <?= $inscrito->tel2?> </p>
                                                        </div>

                                                        <? if (!empty($inscrito->nascimento)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Data de Nascimento</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= fixDate($inscrito->nascimento, false) ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->email)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>E-mail</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->email ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->telefone)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Telefone</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->telefone ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->endereco)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Endereço</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->endereco ?></p>
                                                            </div>

                                                        <? endif ?>

                                                        <? if (!empty($inscrito->numero)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Número</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->numero ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->bairro)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Bairro</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->bairro ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->complemento)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Complemento</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->complemento ?></p>
                                                            </div>
                                                        <? endif ?>

                                                        <? if (!empty($inscrito->cidade_domicilio->nome)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Município</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->cidade_domicilio->nome ?></p>
                                                            </div>
                                                        <? endif ?>


                                                        <div class="col-sm-4">
                                                            <p><strong>Instituição de Ensino</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->instituicao ?></p>
                                                        </div>


                                                        <? if (!empty($inscrito->cidade_polo->nome)): ?>
                                                            <div class="col-sm-4">
                                                                <p><strong>Cidade Polo</strong></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><?= $inscrito->cidade_polo->nome ?></p>
                                                            </div>
                                                        <? endif ?>
                                                        <div class="col-sm-4">
                                                            <p><strong>Nome da  Mãe</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->nome_mae ?></p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <p><strong>Nome do  Pai</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->nome_pai ?></p>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <p><strong>Banco</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->banco ?></p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <p><strong>Conta</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->conta ?></p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <p><strong>Agência</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->agencia ?></p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <p><strong>Operação</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->operacao ?></p>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <p><strong>Variação</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p><?= $inscrito->variacao ?></p>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <legend>Anexos</legend>
                                                        </div>
                                                        
                                                        
                                                        
                                                        <!-------------------------->
                                                        
                                                         <?if(isset($anexos[0])):?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <h3 class="h4"><i class="fa fa-paperclip" aria-hidden="true"></i>  Anexos Inclusos</h3>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Anexo</th>
                                                                    
                                                                    
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <? foreach ($anexos as $a): ?>

                                                                <tr class="tr_anexo" data-name="<?= strstr($a,'_',true) ?>">
                                                                        <td>  
                                                                            <a  target="_blank" href="<?= _url(Inscricao::CAMINHO_DOC . $inscrito->cpf . '/' . $a) ?>"><?= strstr($a,'_',true) ?></a>
                                                                        </td>
                                                                        
                                                                </tr>

                                                           <? endforeach ?>
                                                           </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <?endif?>
                                                        
                                                        
                                                        <!--------------------------->



                                                    </div>




                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <? require_once _file("/admin/templates/part_footer.inc.php"); ?>


            </div>

        </div>
        <? require_once _file("/admin/templates/part_js.inc.php"); ?>

        <script type="text/javascript">
            $(document).ready(function () {




                $(document).on('click', '#classificar', function () {

                    alertify.confirm('Tem certeza que deseja classificar o inscrito?', function () {

                        window.location = "<?= _src('/admin/detalhes.php?go=classificar&id=') . $inscrito->idinscricao ?>";

                    });
                })

                $(document).on('click', '#desclassificar', function () {

                    alertify.confirm('Tem certeza que deseja desclassificar o inscrito?', function () {

                        window.location = "<?= _src('/admin/detalhes.php?go=desclassificar&id=') . $inscrito->idinscricao ?>";

                    });
                })



                $(document).on('click', '#excluir', function () {

                    alertify.confirm('Tem certeza que deseja excluir o inscrito?', function () {

                        window.location = "<?= _src('/admin/detalhes.php?go=excluir&id=') . $inscrito->idinscricao ?>";

                    });
                })





            });
        </script>
    </body>
</html>
