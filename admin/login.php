<?



$__NO_LOGIN = true;

require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('loginIndex');

require_once (_file('/controllers/UsuarioCtrl.php'));

?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" >
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.urbanui.com/amaze/assets/img/apple-icon.png" >
    <link rel="icon" type="image/png" href="<?= _src('/admin/assets/img/favicon.png')?>" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >

    <title>Admin</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' >
    <meta name="viewport" content="width=device-width" >

     <? require_once _file("/admin/templates/part_head.inc.php");?>

</head>

<body>
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
               
                <a class="navbar-brand" href="<?= _src('/admin')?>"> Admin</a>
            </div>
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page"  data-color="rose">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">

                            <form method="post" action="" class="val_not">
                                <div class="card card-login card-hidden">
                                    <div class="header text-center">
                                        <h3 class="title">Login</h3>
                                    </div>
                                    <div class="content">

                                        <div class="form-group">
                                            <label>Email </label>
                                            <input type="email" placeholder="" name="login" class="form-control ">
                                        </div>
                                        <div class="form-group">
                                            <label>Senha</label>
                                            <input type="password" placeholder=""  name="senha" class="form-control ">
                                        </div>
                                    </div>

                                    <input type="hidden" value="login" name="go">

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-default btn-wd btn-lg">Entrar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                   
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="<?= _src('/')?>">MyAdmin</a>
                    </p>
                </div>
            </footer>
        </div>
    </div>

 
 <? require_once _file("/admin/templates/part_js.inc.php");?>


<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);



        <? if (isset($msgUsuarioValido) && !$msgUsuarioValido): ?>
               
                 alertify.error("Senha e usuário não correspondem!");

        <? endif ?>
        <? if (isset($go) && $go == 'logout'): ?>

                 alertify.warning("Você saiu do sistema");
        <? endif ?>
    });
</script>
</body>
</html>
