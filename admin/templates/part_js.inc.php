<!--   Core JS Files   -->
<script src="<?=_src('/admin/assets/vendors/jquery-3.1.1.min.js')?>" type="text/javascript"></script>
<script src="<?=_src('/admin/assets/vendors/jquery-ui.min.js')?>" type="text/javascript"></script>
<script src="<?=_src('/admin/assets/vendors/bootstrap.min.js')?>" type="text/javascript"></script>
<script src="<?=_src('/admin/assets/vendors/material.min.js')?>" type="text/javascript"></script>
<script src="<?=_src('/admin/assets/vendors/perfect-scrollbar.jquery.min.js')?>" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="<?=_src('/admin/assets/vendors/jquery.validate.min.js')?>"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?=_src('/admin/assets/vendors/moment.min.js')?>"></script>
<!--  Charts Plugin -->
<script src="<?=_src('/admin/assets/vendors/charts/flot/jquery.flot.js')?>"></script>
<script src="<?=_src('/admin/assets/vendors/charts/flot/jquery.flot.resize.js')?>"></script>
<script src="<?=_src('/admin/assets/vendors/charts/flot/jquery.flot.pie.js')?>"></script>
<script src="<?=_src('/admin/assets/vendors/charts/flot/jquery.flot.stack.js')?>"></script>
<script src="<?=_src('/admin/assets/vendors/charts/flot/jquery.flot.categories.js')?>"></script>
<script src="<?=_src('/admin/assets/vendors/charts/chartjs/Chart.min.js')?>" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="<?=_src('/admin/assets/vendors/jquery.bootstrap-wizard.js')?>"></script>
<!--  Notifications Plugin    -->
<script src="<?=_src('/admin/assets/vendors/bootstrap-notify.js')?>"></script>
<!-- DateTimePicker Plugin -->
<script src="<?=_src('/admin/assets/vendors/bootstrap-datetimepicker.js')?>"></script>
<!-- Vector Map plugin -->
<script src="<?=_src('/admin/assets/vendors/jquery-jvectormap.js')?>"></script>
<!-- Sliders Plugin -->
<script src="<?=_src('/admin/assets/vendors/nouislider.min.js')?>"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAurmSUEQDwY86-wOG3kCp855tCI8lHL-I"></script>
<!-- Select Plugin -->
<script src="<?=_src('/admin/assets/vendors/jquery.select-bootstrap.js')?>"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="<?=_src('/admin/assets/js/bootstrap-checkbox-radio-switch-tags.js')?>"></script>

<!-- Circle Percentage-chart -->
<script src="<?=_src('/admin/assets/js/jquery.easypiechart.min.js')?>"></script>

<!--  DataTables.net Plugin    -->
<script src="<?=_src('/admin/assets/vendors/jquery.datatables.js')?>"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?=_src('/admin/assets/vendors/sweetalert/js/sweetalert2.min.js')?>"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?=_src('/admin/assets/vendors/jasny-bootstrap.min.js')?>"></script>
<!--  Full Calendar Plugin    -->
<script src="<?=_src('/admin/assets/vendors/fullcalendar.min.js')?>"></script>
<!-- TagsInput Plugin -->
<script src="<?=_src('/admin/assets/vendors/jquery.tagsinput.js')?>"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?=_src('/admin/assets/js/amaze.js')?>"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?=_src('/admin/assets/js/demo.js')?>"></script>

<script src="<?=_src('/admin/assets/js/charts/flot-charts.js')?>"></script>
<script src="<?=_src('/admin/assets/js/charts/chartjs-charts.js')?>"></script>

<script src="<?=_src('/admin/assets/js/myadmin.js')?>"></script>


<script src="//cdn.jsdelivr.net/alertifyjs/1.9.0/alertify.min.js"></script>


<script src="<?= _src('/admin/assets/js/jquery.mask.min.js?2') ?>"></script>
<script src="<?= _src('/admin/assets/js/jquery.validate.min.js?2') ?>"></script>


<script>

    $(function () {
        

        <? if (isset($mensagemSucesso) && !empty($mensagemSucesso)): ?>
            notify ('<?=$mensagemSucesso?>', 'success');
        <? endif; ?>
        <? if (isset($mensagemErro) && !empty($mensagemErro)): ?>
            notify ('<?=$mensagemErro?>', 'error');
        <? endif; ?>


        <? if (isset($codAlert) && !empty($codAlert) ): ?>
            alertify.alert("<?=$msgAlerts[$codAlert][0]?>");
        <? endif; ?>
            
        
       
       
    });

</script>
