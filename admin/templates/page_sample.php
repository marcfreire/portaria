<?

require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('index');

require_once (_file('/controllers/UsuarioCtrl.php'));

?>

<!doctype html>
<html lang="pt-br">

<head>

   <? require_once _file("/pages/templates/part_head.inc.php");?>

</head>

<body>
    <div class="wrapper">
		

		<? require_once _file("/pages/templates/part_sidemenu.inc.php");?>


	        <div class="main-panel">
	           	
	           	<? require_once _file("/pages/templates/part_nav.inc.php");?>
	            

	            <div class="content">
	                <div class="container-fluid">


	                </div>
	            </div>
	          

	           <? require_once _file("/pages/templates/part_footer.inc.php");?>
	        

	        </div>

    </div>
<? require_once _file("/pages/templates/part_js.inc.php");?>

<script type="text/javascript">
	$(document).ready(function(){
		//demo.initStatsDashboard();
		demo.initVectorMap();
		demo.initCirclePercentage();

	});
</script>
</body>
</html>
