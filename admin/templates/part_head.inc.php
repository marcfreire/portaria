

    <meta charset="utf-8" >
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.urbanui.com/amaze/assets/img/apple-icon.png" >
    <link rel="icon" type="image/png" href="<?=_src('/admin/assets/img/favicon.png')?>" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >

    <title>Admin</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' >
    <meta name="viewport" content="width=device-width" >

    <!-- Bootstrap core CSS     -->
    <link href="<?=_src('/admin/assets/css/bootstrap.min.css')?>" rel="stylesheet" >

    <!--  Material Dashboard CSS    -->
    <link href="<?=_src('/admin/assets/css/amaze.css')?>" rel="stylesheet" >

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?=_src('/admin/assets/css/demo.css')?>" rel="stylesheet" >

    <!--     Fonts and icons     -->
    <link href="<?=_src('/admin/assets/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?=_src('/admin/assets/css/font-muli.css')?>" rel='stylesheet' type='text/css'>
    

    <link href="<?=_src('/admin/assets/themify-icons/themify-icons.css')?>" rel="stylesheet">

    <link href="<?=_src('/admin/assets/vendors/sweetalert/css/sweetalert2.min.css')?>" rel="Stylesheet" >

    <link href="<?=_src('/admin/assets/css/myadmin.css')?>" rel="Stylesheet" >



    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.9.0/css/themes/default.min.css"/>