 <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
							<i class="ti-arrow-left"></i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                       

                        <? if (getNameScript() == 'index'): ?>
                            <a class="navbar-brand text-uppercase" href="#.">
                                Início
                            </a>
                        <? elseif(getNameScript() == 'alterar_senha'): ?>
                            <a class="navbar-brand text-uppercase" href="#.">
                                Alterar Senha 
                            </a>
                        <? else: ?>
                            <a class="navbar-brand text-uppercase" href="#.">
                                <?= getNameScript()?> 
                            </a>
                        <? endif ?>

            
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">

                       
                            
						
                            
                             <li class="dropdown">
                                <a href="#." class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="ti-user"></i>
                                 
                                    <p class="">
                                        <?=$usuarioLogado->nome ?>
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?= _src('/admin/alterar_senha.php')?>">Alterar Senha</a>
                                    </li>
                                     <li>
                                        <a href="<?= _src('/admin/login.php?go=logout')?>">Sair</a>
                                    </li>
                                  
                                </ul>
                            </li>
                           
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                    </div>
                </div>
            </nav>