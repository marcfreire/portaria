<div class="sidebar" data-background-color="brown" data-active-color="warning">
	    <!--
			Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
			Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
		-->
            <div class="logo">
                <a href="<?= _src('/admin')?>" class="simple-text">
                    ADMIN
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="<?= _src('/admin')?>" class="simple-text">
                    A
                </a>
            </div>
            <div class="sidebar-wrapper">
				<ul class="nav">
                    <li <?if(getNameScript() == 'index' ):?> class="active" <?endif?> >
                        <a href="<?= _src('/admin')?>">
                            <i class="fa fa-home" aria-hidden="true"></i>

                            <p>Início</p>
                        </a>
                    </li>
					
					
					 
					<li <?if(getNameScript() == 'inscritos' || getNameScript() == 'inscrito' || getNameScript() == 'detalhes' ):?> class="active" <?endif?> >
                        <a href="<?= _src('/admin/inscritos.php')?>">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            <p>Inscritos</p>
                        </a>
                    </li>
                    
                    <li <?if(getNameScript() == 'usuarios' || getNameScript() == 'usuario' || getNameScript() == 'alterar_senha'):?> class="active" <?endif?> >
                        <a data-toggle="collapse" href="#tables" class="collapsed" aria-expanded="false">
                             <i class="fa fa-users" aria-hidden="true"></i>

                            <p>Usuários
                              <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="tables" role="navigation" aria-expanded="false" style="height: 0px;">
                            <ul class="nav">
                                <li <?if(getNameScript() == 'usuarios' ):?> class="active" <?endif?>>
                                    <a href="<?= _src('/admin/usuarios.php')?>" >Todos</a>
                                </li>
                                <li <?if(getNameScript() == 'usuario' ):?> class="active" <?endif?>>
                                    <a href="<?= _src('/admin/usuario.php')?>" >Novo</a>
                                </li>
                                <li <?if(getNameScript() == 'alterar_senha' ):?> class="active" <?endif?>>
                                    <a href="<?= _src('/admin/alterar_senha.php')?>">Alterar Minha Senha</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                
                    <li>
                        <a href="<?= _src('/admin/login.php?go=logout')?>">
                           <i class="fa fa-sign-out" aria-hidden="true"></i>


                            <p>Sair</p>
                        </a>
                    </li>
				
					

					
					
				</ul>

            </div>
        </div>