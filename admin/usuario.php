<?

require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/admin.inc.php';

Controller_defaultMethod('cadastro');

require_once (_file('/controllers/UsuarioCtrl.php'));


?>

<!doctype html>
<html lang="pt-br">

<head>

   <? require_once _file("/admin/templates/part_head.inc.php");?>

</head>

<body>
    <div class="wrapper">
        

        <? require_once _file("/admin/templates/part_sidemenu.inc.php");?>


            <div class="main-panel">
                
                <? require_once _file("/admin/templates/part_nav.inc.php");?>
                

                <div class="content">
                    <div class="container-fluid" >
                     <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <form method="post" <?if (empty($usuario->idusuario)):?> class="val_not"  onsubmit="return validate()" id="form1" <?endif?> >
                                    
                                    <div class="content">
                                        <div class="btn-group">

                                            <a href="<?= _src('/admin/usuarios.php')?>" class="btn btn-default">
                                                <span class="btn-label">
                                                    <i class="ti-search"></i>
                                                </span>
                                                Pesquisar
                                            </a>
                                         
                                           
                                        </div>
                                    </div>
                                    
                                    <div class="header">
                                    
                                        <legend >
                                            Cadastro de Usuário

                                            <? if (!empty($usuario->idusuario)): ?>   
                                                <button type="button" data-id="<?= $usuario->idusuario?>" class="btn btn-danger btn-fill excluir-usuario" style="float: right; margin-top: -12px; margin-right: 10px;">
                                                    <span class="btn-label">
                                                        <i class="ti-trash"></i>
                                                    </span>
                                                    Excluir
                                                </button>
                                            <?endif?>
                                        </legend>
                                    </div>
                                    <div class="content">
                                        
                                        <div class="form-group">
                                            <label>Nome*</label>
                                            <input type="text" class="v_req form-control" <?=formInput('usuario->nome')?> >
                                        </div>

                                        <div class="form-group">
                                            <label>Email*</label>
                                            <input type="email" <?=formInput('usuario->email')?> class=" v_req v_mail form-control">
                                        </div>
                                        
                                        <?if (empty($usuario->idusuario)):?>

                                        <div class="form-group">
                                            <label>Senha*</label>
                                            <input type="password" name="usuario->senha" id="usuario_senha" class=" v_req  form-control">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Confirme sua senha*</label>
                                            <input type="password" name="senha_confirma" id="senha_confirma" class=" v_req  form-control">
                                        </div>

                                        
                                        <?endif?>
                                      

                                        <input type='hidden' <?= formInput('usuario->idusuario') ?>/>
                                         
                                        <input type="hidden" name="go" value="save">
                                        <button type="submit" class="btn btn-fill btn-success">Salvar</button>
                                    </div>
                                </form>
                            </div> <!-- end card -->
                        </div> <!--  end col-md-6  -->
                     </div>
                 


                    </div>
                </div>
              

               <? require_once _file("/admin/templates/part_footer.inc.php");?>
            

            </div>

    </div>
<? require_once _file("/admin/templates/part_js.inc.php");?>

<script type="text/javascript">
    $(document).ready(function(){
        

        <? if (!empty($usuario->idusuario)): ?>



        $(document).on('click', '.excluir-usuario', function(event) {
            event.preventDefault();

            id = $(this).data('id');

            alertify.confirm('MyAdmin', 'Tem certeza que deseja excluir esse usuario ? ', 
                function() { 

                    window.location = "<?= _src('/myadmin/usuarios.php?go=excluir&id=')?>"+id;
                }
                , function(){ 
            
                }
            );
          
        });

        <?endif?>


    });
 
 <?if (empty($usuario->idusuario)):?>

       function validate() {


            
                 val = $("#form1").valid();


                    if (val) {
                        if ($('#usuario_senha').val() === $('#senha_confirma').val()) {

                            return true;
                        } else {
                       
                            alertify.error('Senhas não correspondem');
                            $('#usuario_senha').closest('.form-group').addClass("has-error");
                            $('#senha_confirma').closest('.form-group').addClass("has-error");
                          
                            return false;
                        }
                    }else{
                      alertify.error('Aluguns campos não foram preenchidos');
                    
                    }

                    return false;

                }

            <?endif?>
</script>
</body>
</html>
