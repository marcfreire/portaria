<?php

require_once __DIR__ . '/../myconfig.php';

require_once (_file('/lib/myphp/ConnectFactory.class.php'));
require_once (_file('/lib/myphp/DAO.class.php'));
require_once (_file('/dao/InscricaoDAO.class.php'));
require_once (_file('/models/Inscricao.class.php'));


$inscricaoDAO = new InscricaoDAO;

$funcao = filter_input(INPUT_POST, 'funcao');

if(isset($funcao) && $funcao === "validacaoDistanciaNis"){

    $json = [];

    $limit = filter_input(INPUT_POST, 'limit', FILTER_VALIDATE_INT);
    $offset = filter_input(INPUT_POST, 'offset', FILTER_VALIDATE_INT);

    $inscricao = DAO::querySimple("select i.*,c1.nome as cidade_domicilio_nome,
    c2.nome as cidade_polo_nome, ins.nome as instituicao_nome
    from inscricao i 
    inner join cidade c1 on i.cidade_domicilio=c1.idcidade 
    inner join cidade c2 on i.cidade_polo=c2.idcidade 
    inner join instituicao ins on i.idinstituicao = ins.idinstituicao
    where i.stdelete=1 order by datacadastro limit $limit offset $offset",false);



    if(isset($inscricao[0])){
        validacaoDistanciaNis($inscricao);
        $json['final'] = false;
    }else{
        $json['final'] = true;
    }
    
    echo json_encode( $json);

}

function validacaoDistanciaNis( $result){

    

        $inscricao =  $result[0];

        $inscricao->distancia = calculaDistancia($inscricao->cidade_domicilio_nome, $inscricao->cidade_polo_nome);
        $inscricao = verificaNis($inscricao);

        $inscricao = DAO::fetch('Inscricao', $inscricao);

        DAO::merge($inscricao);

}

function verificaNis($inscricao){

    $url  = 'http://191.253.67.150/api.php';
    $data1 = ['tipo' => '1','cpf'=>limpaCPF_CNPJ($inscricao->cpf),'rg'=>limpaCPF_CNPJ($inscricao->rg),'chave'=>'SECTI20170914'];
     
    $resultAPI = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '',requestAPI($data1,$url)), true );

    if($resultAPI['acao'] == 'OK'){
        $inscricao->nis_localizado = 1;
    }else{
        $inscricao->nis_localizado = 0;
    }

    $data2 = ['tipo' => '2','nis'=>limpaCPF_CNPJ($inscricao->nis),'chave'=>'SECTI20170914'];
    $resultAPI = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '',requestAPI($data2,$url)), true );

    if($resultAPI['acao'] == 'OK'){
        $inscricao->nis_localizado = 1;
        $inscricao->nis_nome = $resultAPI['nome'];
        $inscricao->nis_cpf = $resultAPI['cpf'] ;
        $inscricao->nis_rg = $resultAPI['rg'] ;
    }else{
        $inscricao->nis_valido = 0;  
    }    

    return $inscricao;
    
}

function calculaDistancia( $cidade_domicilio,$cidade_polo ){

    $result = [];
    
    $origin = str_replace(' ', '%20', $cidade_domicilio." , Maranhão");
    $destino = str_replace(' ', '%20', $cidade_polo." , Maranhão");

    $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destino&mode=driving&language=pt-BR&sensor=false";

    $data = @file_get_contents($url);

    $result = json_decode($data, true);
  

    $str = explode(" ", $result['rows'][0]['elements'][0]['distance']['text'] );

    $distancia = str_replace(",", ".", $str[0]);


    return $distancia;

}

function requestAPI($postdata, $url){

    $postdata = http_build_query($postdata);
    
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    
    $context  = stream_context_create($opts);
    
    $result = file_get_contents($url, false, $context);

    return $result;

}

function limpaCPF_CNPJ($valor){
    $valor = trim($valor);
    $valor = str_replace(".", "", $valor);
    $valor = str_replace(",", "", $valor);
    $valor = str_replace("-", "", $valor);
    $valor = str_replace("/", "", $valor);
    return $valor;
}