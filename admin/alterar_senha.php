<?

require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/admin.inc.php';

Controller_defaultMethod('alteraSenha');

require_once (_file('/controllers/UsuarioCtrl.php'));

?>

<!doctype html>
<html lang="pt-br">

<head>

   <? require_once _file("/admin/templates/part_head.inc.php");?>

</head>

<body>
    <div class="wrapper">
		

		<? require_once _file("/admin/templates/part_sidemenu.inc.php");?>


	        <div class="main-panel">
	           	
	           	<? require_once _file("/admin/templates/part_nav.inc.php");?>
	            

	            <div class="content">
	                <div class="container-fluid">
	                 <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
							    <form method="post" class="val_not"  onsubmit="return validate()" id="form1" >
							    	
							    	
		                            <div class="header">
                                    
									    <h4 class="title">
											Alterar Senha
										</h4>
									</div>
		                            <div class="content">
	                                    
	                                    <div class="form-group">
                                            <label>Senha*</label>
                                            <input type="password" name="senha_nova" id="senha_nova" class=" v_req  form-control">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Confirme sua senha*</label>
                                            <input type="password" name="senha_confirma" id="senha_confirma" class=" v_req  form-control">
                                        </div>

	                                
	                                    <input type="hidden" name="go" value="alterarSenha">
	                                    <button type="submit" class="btn btn-fill btn-success">Atualizar</button>
		                            </div>
							    </form>
	                        </div> <!-- end card -->
	                    </div> <!--  end col-md-6  -->
	                 </div>
                 


	                </div>
	            </div>
	          

	           <? require_once _file("/admin/templates/part_footer.inc.php");?>
	        

	        </div>

    </div>
<? require_once _file("/admin/templates/part_js.inc.php");?>

<script type="text/javascript">
	$(document).ready(function(){
		

	});


	  function validate() {


            
             val = $("#form1").valid();


                if (val) {
                    if ($('#senha_nova').val() === $('#senha_confirma').val()) {

                        return true;
                    } else {
                   
                        alertify.error('Senhas não correspondem');
                        $('#senha_nova').closest('.form-group').addClass("has-error");
                        $('#senha_confirma').closest('.form-group').addClass("has-error");
                      
                        return false;
                    }
                }else{
                  alertify.error('Aluguns campos não foram preenchidos');
                
                }

                return false;

            }


</script>
</body>
</html>
