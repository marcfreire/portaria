<?

require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/admin.inc.php';

Controller_defaultMethod('cadastro');

require_once (_file('/controllers/InscricaoCtrl.php'));


?>

<!doctype html>
<html lang="pt-br">

<head>

   <? require_once _file("/admin/templates/part_head.inc.php");?>

</head>

<body>
    <div class="wrapper">
		

		<? require_once _file("/admin/templates/part_sidemenu.inc.php");?>


	        <div class="main-panel">
	           	
	           	<? require_once _file("/admin/templates/part_nav.inc.php");?>
	            

	            <div class="content">
	                <div class="container-fluid">
	                 <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
							    <form method="post" >
							    	
							    	<div class="content">
							    		<div class="btn-group">

							    			<a href="<?= _src('/inscritos')?>" class="btn btn-default">
		                                        <span class="btn-label">
		                                            <i class="ti-search"></i>
		                                        </span>
		                                        Pesquisar
		                                    </a>
		                                    <a href="<?= _src('/inscrito')?>" class="btn btn-info">
		                                        <span class="btn-label">
		                                            <i class="ti-plus"></i>
		                                        </span>
		                                        Novo Inscrito
		                                    </a>
	                                     
	                                     
	                                       
	                                    </div>
							    	</div>
							     	
		                            <div class="header">
                                    
									    <h4 class="title">
											Cadastro de Aluno

											 <? if (!empty($inscricao->idinscricao)): ?> 
                                              <button type="button" data-id="<?= $inscricao->idinscricao?>" class="btn btn-danger btn-fill excluir-inscricao" style="float: right; margin-top: -12px; margin-right: 10px;">
                                                  <span class="btn-label">
                                                      <i class="ti-trash"></i>
                                                  </span>
                                                  Excluir
                                              </button>
                                           <?endif?>
										</h4>
									</div>
		                            <div class="content">
                                         

                                         <div class="row">
                                         		<div class="form-group col-md-8">
			                                        <label>Nome*</label>
			                                        <input type="text" class="form-control v_req" <?= formInput('inscricao->nome')?> >
			                                    </div>
			                                    
			                                    <div class="form-group col-md-4">
			                                        <label>CPF*</label>
			                                        <input type="text" class="form-control v_req maskcpf" <?= formInput('inscricao->cpf')?> >
			                                    </div>
                                         </div>

                                     
		                                   

                                         
		                                   

		                              

	                                  
                                        
	                                   

	                                    <input type='hidden' <?= formInput('inscricao->idinscricao') ?>/>
	                                    <input type="hidden" name="go" value="save">
	                                    <button type="submit" class="btn btn-fill btn-success">Salvar</button>
		                            </div>
							    </form>
	                        </div> <!-- end card -->
	                    </div> <!--  end col-md-6  -->
	                 </div>
                 


	                </div>
	            </div>
	          

	           <? require_once _file("/admin/templates/part_footer.inc.php");?>
	        

	        </div>

    </div>
<? require_once _file("/admin/templates/part_js.inc.php");?>

<script type="text/javascript">
	$(document).ready(function(){
          

          <? if (!empty($inscricao->idinscricao)): ?>

          $(document).on('click', '.excluir-inscricao', function(event) {
              event.preventDefault();

              id = $(this).data('id');

              alertify.confirm('Admin', 'Tem certeza que deseja excluir esse inscrito ? ', 
                  function() { 

                      window.location = "<?= _src('/admin/inscritos.php?go=excluir&id=')?>"+id;
                  }
                  , function(){ 
              
                  }
              );
            
          });

        <?endif?>

	
		

	});
</script>
</body>
</html>
