<?php

require_once __DIR__.'/../myconfig.php';


/**
 *
 * @Table("cidade");
 *
 *
 */
class Cidade {

    use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idcidade;
    
     /**
     * @Column
     */
    public $uf;

    /**
     * @Column
     */
    public $status;
    
     /**
     * @Column
     */
    public $nome;

  

    public function __construct() {
        
    }

}
