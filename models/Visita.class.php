<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 * @Table("visita");
 *
 *
 */
class Visita {

    use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idvisita;

    /**
     *
     * @Column
     */
    public $tipo_instituicao;

    /**
     *
     * @Column
     */
    public $instituicao;
    
    
    /**
     *
     * @Column
     */
    public $stdelete;
    
    
     /**
     *
     * @Column
     */
    public $notificada;
    
    
     /**
     *
     * @Column
     */
    public $datacadastro;
    
    
     /**
     *
     * @Relation(target="Setor",column="idsetor")
     */
    public $setor;
    
    
     /**
     *
     * @Relation(target="Inscricao",column="idinscricao")
     */
    public $inscricao;

    /**
     * @Column
     */
    public $checkout;

    public function __construct($instance = true) {

        if ($instance) {
            
             $this->setor = new Setor;
             $this->inscricao = new Inscricao;
            
        }
    }

}
