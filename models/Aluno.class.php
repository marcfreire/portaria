<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 * @Table("aluno");
 *
 *
 */
class Aluno {

    use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idaluno;

    /**
     * @Column
     */
    public $stdelete;

    /**
     *
     * @Column
     */
    public $datacadastro;

    /**
     * @Column
     */
    public $status;

    /**
     * @Column
     */
    public $tipo_cota;

    /**
     *
     * @Column
     */
    public $matricula;

    /**
     *
     * @Column
     */
    public $data_matricula;

    /**
     *
     * @Column
     */
    public $nome;

    /**
     *
     * @Column
     */
    public $nome_social;

    /**
     *
     * @Column
     */
    public $nome_pai;

    /**
     *
     * @Column
     */
    public $nome_mae;

    /**
     *
     * @Column
     */
    public $bolsa_familia;

    /**
     *
     * @Column
     */
    public $nis;

    /**
     *
     * @Column
     */
    public $sexo;

    /**
     *
     * @Column
     */
    public $nascimento;

    /**
     *
     * @Column
     */
    public $peso;

    /**
     *
     * @Column
     */
    public $altura;

    /**
     *
     * @Relation(target="Estado", column="idestado")
     */
    public $estado;

    /**
     *
     * @Relation(target="Cidade", column="idcidade")
     */
    public $cidade;

    /**
     *
     * @Relation(target="Pais", column="idpais")
     */
    public $pais;

    /**
     *
     * @Relation(target="Unidade", column="idunidade")
     */
    public $unidade;

    /**
     *
     * @Relation(target="Curso", column="idcurso")
     */
    public $curso;

    /**
     *
     * @Relation(target="AnoLetivo", column="idano_letivo")
     */
    public $ano_letivo;

    /**
     *
     * @Relation(target="Turma", column="idturma")
     */
    public $turma;

    /**
     *
     * @Column
     */
    public $rematricula;

    /**
     *
     * @Column
     */
    public $conjuge;

    /**
     *
     * @Column
     */
    public $obs;

    /**
     *
     * @Column
     */
    public $ano_ingresso;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="cor")
     */
    public $cor;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="grupo_sanguineo")
     */
    public $grupo_sanguineo;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="estado_civil")
     */
    public $estado_civil;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="situacao_militar")
     */
    public $situacao_militar;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="nacionalidade")
     */
    public $nacionalidade;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="religiao")
     */
    public $religiao;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="tamanho_farda")
     */
    public $tamanho_farda;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="tamanho_farda2")
     */
    public $tamanho_farda2;

    /**
     *
     * @Column
     */
    public $identidade_numero;

    /**
     *
     * @Column
     */
    public $identidade_digito;

    /**
     *
     * @Column
     */
    public $identidade_data;

    /**
     *
     * @Column
     */
    public $identidade_uf;

    /**
     *
     * @Column
     */
    public $identidade_orgao;

    /**
     *
     * @Column
     */
    public $cpf;

    /**
     *
     * @Column
     */
    public $certidao_numero;

    /**
     *
     * @Column
     */
    public $certidao_folha;

    /**
     *
     * @Column
     */
    public $certidao_livro;

    /**
     *
     * @Column
     */
    public $certidao_data;

    /**
     *
     * @Column
     */
    public $certidao_uf;

    /**
     *
     * @Column
     */
    public $passaporte;

    /**
     *
     * @Column
     */
    public $passaporte_emissao;

    /**
     *
     * @Column
     */
    public $passaporte_validade;

    /**
     *
     * @Column
     */
    public $doc_certificado;

    /**
     *
     * @Column
     */
    public $doc_historico;

    /**
     *
     * @Column
     */
    public $doc_certidao;

    /**
     *
     * @Column
     */
    public $doc_laudo;

    /**
     *
     * @Column
     */
    public $doc_identidade;

    /**
     *
     * @Column
     */
    public $doc_cpf;

    /**
     *
     * @Column
     */
    public $doc_foto;

    /**
     *
     * @Column
     */
    public $doc_residencia;

    /**
     *
     * @Column
     */
    public $doc_declaracao;

    /**
     *
     * @Column
     */
    public $anexo_certificado;

    /**
     *
     * @Column
     */
    public $anexo_laudo;

    /**
     *
     * @Column
     */
    public $anexo_historico;

    /**
     *
     * @Column
     */
    public $anexo_certidao;

    /**
     *
     * @Column
     */
    public $anexo_identidade;

    /**
     *
     * @Column
     */
    public $anexo_cpf;

    /**
     *
     * @Column
     */
    public $anexo_foto;

    /**
     *
     * @Column
     */
    public $anexo_residencia;

    /**
     *
     * @Column
     */
    public $anexo_declaracao;

    /**
     *
     * @Column
     */
    public $historico_patologias;

    /**
     *
     * @Column
     */
    public $obs_patologias;

    /**
     *
     * @Column
     */
    public $intolerancia_alimentar;

    /**
     *
     * @Column
     */
    public $seguro_saude;

    /**
     *
     * @Column
     */
    public $medico_credenciado;

    /**
     *
     * @Column
     */
    public $hospital_credenciado;

    /**
     *
     * @Column
     */
    public $hospital_indicado_pela_familia;

    /**
     *
     * @Column
     */
    public $posto_saude;

    /**
     *
     * @Column
     */
    public $alergia_medicamentosa;

    /**
     *
     * @Column
     */
    public $alergia_medicamentosa_desc;

    /**
     *
     * @Column
     */
    public $medicacao_controlada;

    /**
     *
     * @Column
     */
    public $medicacao_controlada_desc;

    /**
     *
     * @Column
     */
    public $emergencia_parentesco1;

    /**
     *
     * @Column
     */
    public $emergencia_contato1;

    /**
     *
     * @Column
     */
    public $emergencia_parentesco2;

    /**
     *
     * @Column
     */
    public $emergencia_contato2;

    /**
     *
     * @Column
     */
    public $dado_saude_estudante_escola;

    /**
     *
     * @Relation(target="Endereco",column="idendereco")
     */
    public $endereco;

    /**
     *
     * @Relation(target="Colaborador",  column="ultima_atualizacao",attribute="idcolaborador")
     */
    public $ultima_atualizacao;

    /**
     *
     * @Relation(target="AnoLetivo",attribute="idano_letivo",column="selecao_ano")
     */
    public $selecao_ano;

    /**
     *
     * @Column
     */
    public $classificacao_pontos;

    /**
     *
     * @Column
     */
    public $classificacao_posicao_epera;

    /**
     *
     * @Column
     */
    public $apto_matricula;

    /**
     * @Column
     */
    public $cota_aprovado;

    /**
     * @Column
     */
    public $matricula_responsavel;

    /**
     * @Column
     */
    public $matricula_obs;

    /**
     * @Column
     */
    public $turma_numero;

    /**
     * @Column
     */
    public $turma_grupo;

    /**
     * @Column
     */
    public $email;

    /**
     *
     * @Column
     */
    public $contato_numero_parentesco1;

    /**
     *
     * @Column
     */
    public $contato_numero_parentesco2;

    /**
     *
     * @Column
     */
    public $contato_nome_parentesco1;

    /**
     *
     * @Column
     */
    public $contato_nome_parentesco2;

    const STATUS_CLASSIFICADO = 1;
    const STATUS_CLASSIFICADO_EXCEDENTE = 2;
    const STATUS_DESCLASSIFICADO = 3;
    const STATUS_MATRICULADO = 4;
    const STATUS_INATIVO = -1;
    const STATUS_MATRICULA_CANCELADA = -2;
    #
    const COTA_AMPLA = 1;
    const COTA_ESPECIAL = 2;
    const COTA_PUBLICA = 3;
    const COTA_ESPECIAL_PUBLICA = 4;
    #
    const CAMINHO_DOC = '/upload/documentacao/';
    const GRUPO_A = 1;
    const GRUPO_B = 2;

    public function __construct($instance = true) {

        if ($instance) {

            $this->cidade = new Cidade;
            $this->estado = new Estado;
            $this->pais = new Pais;
            $this->unidade = new Unidade;
            $this->nacionalidade = new Metadados;
            $this->cor = new Metadados;
            $this->grupo_sanguineo = new Metadados;
            $this->estado_civil = new Metadados;
            $this->situacao_militar = new Metadados;
            $this->religiao = new Metadados;
            $this->endereco = new Endereco;
            $this->curso = new Curso;
            $this->ano_letivo = new AnoLetivo;
            $this->turma = new Turma;
            $this->selecao_ano = new AnoLetivo();
            $this->tamanho_farda = new Metadados;
            $this->tamanho_farda2 = new Metadados;
            $this->ultima_atualizacao = new Colaborador(false);
        }
    }

}
