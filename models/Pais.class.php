<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 * @Table("pais");
 *
 *
 */
class Pais {
    
     use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idpais;

    /**
     * @Column
     */
    public $status;

    /**
     *
     * @Column
     */
  
    public $sigla;

     /**
     *
     * @Column
     */
    public $nome;

  

    public function __construct($instance = true) {

        if ($instance) {
           
        }
    }

   
   

}

