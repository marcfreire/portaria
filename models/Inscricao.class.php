<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 * @Table("inscricao");
 *
 *
 */
class Inscricao {

    use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idinscricao;

    /**
     * @Column
     */
    public $stdelete;

    /**
     * @Column
     */
    public $datacadastro;

    /**
     * @Column
     */
    public $nome;

    /**
     * @Column
     */
    public $cargo;

    /**
     * @Column
     */
    public $foto;

    /**
     * @Column
     */
    public $cpf;

    /**
     *
     * @Relation(target="Metadados",attribute="idmetadados",column="nacionalidade")
     */
    public $nacionalidade;

    /**
     * @Column
     */
    public $status;

    /**
     * @Column
     */
    public $status_cadastro;

    /**
     * @Column
     */
    public $rg;

    /**
     * @Column
     */
    public $email;

    /**
     * @Column
     */
    public $nascimento;

    /**
     * @Column
     */
    public $sexo;

    /**
     * @Column
     */
    public $tel1;

    /**
     * @Column
     */
    public $tel2;

    /**
     * @Column
     */
    public $cep;

    /**
     * @Column
     */
    public $endereco;

    /**
     * @Column
     */
    public $numero;

    /**
     * @Column
     */
    public $bairro;

    /**
     * @Column
     */
    public $complemento;

    /**
     *
     * @Relation(target="Estado", column="idestado")
     */
    public $estado;

    /**
     *
     * @Relation(target="Cidade", column="idcidade")
     */
    public $cidade;

    /**
     *
     * @Relation(target="Setor", column="idsetor")
     */
    public $setor;

    const STATUS_PENDENTE = 1;
    const STATUS_CLASSIFICADO = 2;
    const STATUS_DESCLASSIFICADO = 3;


    #..
    const STATUS_CADASTRO_PENDENTE = 1;
    const STATUS_CADASTRO_CONCLUIDO = 2;
    #.
    const CAMINHO_DOC = "/upload/documentos/";

    public function __construct() {


        $this->cidade = new Cidade(false);
        $this->setor = new Setor(false);
        $this->estado = new Estado(false);

        $this->nacionalidade = new Metadados;
    }

}
