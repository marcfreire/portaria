<?php
require_once __DIR__.'/../myconfig.php';

/**
 *
 * @Table("estado");
 *
 *
 */
class Estado {
    
     use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idestado;

    /**
     * @Column
     */
    public $nome;

    /**
     * @Column
     */
    public $status;

    /**
     * @Column
     */
    public $uf;

    public function __construct() {
        
    }

    

}
