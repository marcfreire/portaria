<?php

require_once __DIR__.'/../myconfig.php';


/**
 *
 * @Table("instituicao");
 *
 *
 */
class Instituicao {

    use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idinstituicao;
    
     /**
     * @Column
     */
    public $stdelete;

    /**
     * @Column
     */
    public $datacadastro;
    
     /**
     * @Column
     */
    public $nome;

  

    public function __construct() {
        
    }

}
