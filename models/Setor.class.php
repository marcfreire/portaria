<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 * @Table("setor");
 *
 *
 */
class Setor {
    
     use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idsetor;

   

     /**
     *
     * @Column
     */
    public $nome;

     /**
     *
     * @Column
     */
    public $stdelete;
    
    
    /**
     *
     * @Column
     */
    public $portaria;
    
     /**
     *
     * @Relation(target="Setor",attribute="idsetor",column="setormestre")
     */
    public $setormestre;


  

    public function __construct($instance = true) {

        if ($instance) {
           $this->setormestre = new Setor(false);
        }
    }

   
   

}

