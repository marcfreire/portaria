<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 * @Table("usuario");
 *
 *
 */
class Usuario {

    use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idusuario;

    /**
     * @Column
     */
    public $stdelete;

    /**
     *
     * @Relation(target="Setor",column="idsetor")
     */
    public $setor;

    /**
     * @Column
     */
    public $datacadastro;

    /**
     * @Column
     */
    public $nome;
    
    
    /**
     * @Column
     */
    public $foto;

    /**
     * @Column
     */
    public $email;

    /**
     * @Column
     */
    public $senha;

    public function __construct() {
        
        $this->setor = new Setor;
        
    }

}
