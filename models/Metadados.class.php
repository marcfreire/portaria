<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 * @Table("metadados");
 *
 *
 */
class Metadados {

    use Accessors;

    /**
     *
     * @Id
     * @Column
     * @AutoGenerator
     */
    public $idmetadados;

    /**
     * @Column
     */
    public $descricao;

    /**
     * @Column
     */
    public $descricao2;

    /**
     * @Column
     */
    public $stdelete;

    /**
     * @Column
     */
    public $codigo;

    const TIPO_UNIDADE = 1;
    const COR = 2;
    const GRUPO_SANGUINEO = 3;
    const ESTADO_CIVIL = 4;
    const GRAU_DE_INSTRUCAO = 5;
    const SITUACAO_MILITAR = 6;
    const MODALIDADE = 7;
    const NIVEL = 8;
    const TURNO = 10;
    #const ANO_LETIVO = 11;
    const NACIONALIDADE = 12;
    const RELIGIAO = 13;
    const GRUPO_SERVIDOR = 14;
    const EIXO_TECNOLOGICO = 15;
    const AREA_CONHECIMENTO = 16;
    const TAMANHO_FARDA = 17;
    const TAMANHO_FARDA2 = 18;
    const JUSTIFICATIVA_ABONO_FALTA = 19;
    const JUSTIFICATIVA_RETIFICAR_NOTA = 20;
    const MODALIDADE_CONTRATO = 21;
    const BANCO = 22;
    const TIPO_AMBIENTE = 23;


    public static $dados = [
        Metadados::TIPO_UNIDADE => [
            'nome' => 'Tipo de unidade',
        ],
        Metadados::COR => [
            'nome' => 'Cor/Etnia',
        ],
        Metadados::GRUPO_SANGUINEO => [
            'nome' => 'Grupo sanguíneo',
        ],
        Metadados::ESTADO_CIVIL => [
            'nome' => 'Estado Civil',
        ],
        Metadados::GRAU_DE_INSTRUCAO => [
            'nome' => 'Grau de instrução',
        ],
        Metadados::SITUACAO_MILITAR => [
            'nome' => 'Situação militar',
        ],
        Metadados::MODALIDADE => [
            'nome' => 'Modalidade',
        ],
        Metadados::NIVEL => [
            'nome' => 'Nível',
        ],
        Metadados::TURNO => [
            'nome' => 'Turno',
        ],
        Metadados::NACIONALIDADE => [
            'nome' => 'Nacionalidade',
        ],
        Metadados::RELIGIAO => [
            'nome' => 'Religião',
        ],
        Metadados::GRUPO_SERVIDOR => [
            'nome' => 'Cargo/Função',
        ],
        Metadados::EIXO_TECNOLOGICO => [
            'nome' => 'Eixo Tecnológico',
        ],
        Metadados::AREA_CONHECIMENTO => [
            'nome' => 'Área de Conhecimento',
        ],
        Metadados::TAMANHO_FARDA => [
            'nome' => 'Tamanho da Farda - Camisa',
        ],
        Metadados::TAMANHO_FARDA2 => [
            'nome' => 'Tamanho da Farda - Calça',
        ],
        Metadados::JUSTIFICATIVA_ABONO_FALTA=> [
            'nome' => 'Justificativa para abono de falta',
        ],
        Metadados::JUSTIFICATIVA_RETIFICAR_NOTA => [
            'nome' => 'Justificativa retificação de nota',
        ],
        Metadados::MODALIDADE_CONTRATO => [
            'nome' => 'Vínculo funcional',
        ],
         Metadados::BANCO => [
            'nome' => 'Banco',
        ],
        Metadados::TIPO_AMBIENTE => [
            'nome' => 'Tipo Ambiente',
        ]
    ];

}
