<?php

require_once __DIR__ . '/../myconfig.php';

/**
 * Description of controllers
 *
 * @author marcos
 */
Class Controllers {

    public $modo;

    public function __construct() {

        $this->usuarioSessao = UsuarioDAO::loadUserSession(false);
        $this->usuarioLogado = clone $this->usuarioSessao;
        $this->usuarioTaLogado = UsuarioDAO::isUserLogged();


        //para o inscript
        $this->inscricaoSessao = InscricaoDAO::loadUserSession(false);
        
        
       
        
        $this->inscricaoLogado = clone $this->inscricaoSessao;

        $this->inscricaoTaLogado = InscricaoDAO::isUserLogged();

        if ((isset($this->inscricaoLogado->senha_recuperacao) && $this->inscricaoLogado->senha_recuperacao ) && getNameScript() !== 'alterar_senha') {

          ;
            header('Location:' . _src('/alterar_senha?force=true' ));
        }
    }

}
