<?php

require_once __DIR__ . '/../myconfig.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * 
 *
 * @author Geanderson
 */
class UsuarioCtrl extends Controllers {

    public function __construct() {

        parent::__construct();

        $this->usuario = new Usuario;
        $this->usuarioDAO = new UsuarioDAO;
        $this->inscricao = new Inscricao;
        $this->setorDAO = new SetorDAO();
    }

    public function __initialize() {

        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

        if ($id) {

            $this->usuario = DAO::load('Usuario', $id);

            $this->usuario->setor->idsetor = $this->usuario->others->idsetor;
        }
    }

    public function index() {

        $this->listaSetores = $this->setorDAO->lista();






        dispatcher();
    }

    public function loginIndex() {


        dispatcher();
    }

    public function login() {

        $login = filter_input(INPUT_POST, 'login');
        $senha = filter_input(INPUT_POST, 'senha');



        $usuario = $this->usuarioDAO->login($login, $senha);

        if ($usuario) {


            UsuarioDAO::saveUserSession($usuario);

            header('Location:' . _src('/'));
        } else {
            $this->msgUsuarioValido = false;
        }

        # var_dump($usuario);

        dispatcher();
    }

    public function usuarios() {


        $this->setores = DAO::querySimple('select * from usuario where stdelete=1 ', false);


        dispatcher();
    }

    public function delete() {



        $this->usuario->stdelete = 0;

        DAO::merge($this->usuario, ['stdelete']);
        $this->mensagemSucesso = MSG_NOTIFICACAO_EXCLUIR_SUCESSSO;


        $this->usuarios();
    }

    public function create() {


        if (!empty($_FILES['foto']['name'])) {
            
            $file = $_FILES['foto'];        
            $nome =  uniqid(rand(), true) . '.' . strtolower(getTypeFile($file));
            if (!empty($this->usuario->others->foto)) {
                unlink(_file('/upload/fotos/') . $this->usuario->others->foto);
            }
            move_uploaded_file($file['tmp_name'], _file('/upload/fotos/') . $nome);
            $this->usuario->foto = $nome;
            UsuarioDAO::loadUserSession()->others->foto = $nome;
            
        }

        if (empty($this->usuario->idusuario)) {

            $this->usuario->senha = md5($this->usuario->senha);
        }



        DAO::merge($this->usuario);

        $this->mensagemSucesso = MSG_NOTIFICACAO_SALVO_SUCESSSO;

        $this->index();
    }

    public function logout() {

        UsuarioDAO::logout();


        dispatcher();
    }

    public function pesquisa() {

        $this->listaUsuario = DAO::lista('Usuario', ['stdelete' => 1]);


        dispatcher();
    }

    public function alteraSenha() {


        dispatcher();
    }

    public function alterarSenha() {

        $user = UsuarioDAO::loadUserSession();

        $user->senha = md5($this->usuario->senha);

        DAO::merge($user, ['senha']);

        $this->mensagemSucesso = 'Senha alterada com sucesso!';

        dispatcher();
    }

}

Controller_execute("UsuarioCtrl");

require_once (_file('/lib/myphp/dispatcher.inc.php'));
