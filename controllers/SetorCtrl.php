<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 *
 * @author Geanderson
 */
class SetorCtrl extends Controllers {

    public $setor;

    public function __construct() {




        parent::__construct();

        $this->setor = new Setor();
    }

    public function __initialize() {

        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

        if ($id) {

            $this->setor = DAO::load('Setor', $id);
            $this->setor->setormestre = new Setor;
            $this->setor->setormestre->idsetor = $this->setor->others->setormestre;
        }
    }

    public function setores() {


        $this->setores = DAO::querySimple('select s.*,m.nome as setormestre_nome  from setor as s left join setor as m on m.idsetor = s.setormestre  where s.stdelete=1 ', false);




        dispatcher();
    }

    public function delete() {



        $this->setor->stdelete = 0;



        DAO::merge($this->setor, ['stdelete']);
        $this->mensagemSucesso = MSG_NOTIFICACAO_EXCLUIR_SUCESSSO;


        $this->setores();
    }

    public function create() {



        DAO::merge($this->setor);

        $this->mensagemSucesso = MSG_NOTIFICACAO_SALVO_SUCESSSO;

        $this->index();
    }

    public function index() {

        $this->listaSetores = DAO::querySimple('select * from setor where stdelete=1 and setormestre is null', false);
        dispatcher();
    }

}

Controller_execute("SetorCtrl");

require_once (_file('/lib/myphp/dispatcher.inc.php'));
