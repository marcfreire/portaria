<?php

require_once __DIR__ . '/../myconfig.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * 
 *
 * @author Geanderson
 */
class InscricaoCtrl extends Controllers {

    public function __construct() {

        parent::__construct();

        $this->inscricao = new Inscricao;
        $this->inscricaoDAO = new InscricaoDAO;
        $this->metaDados = new MetadadosDAO();
        $this->visita = new Visita();
        $this->visitaDAO = new VisitaDAO();
        $this->setorDAO = new SetorDAO();


        $this->anexosObrigatorios = [];

        $this->anexos = [];
    }

    public function __initialize() {

        if (isset(InscricaoDAO::loadUserSession()->cpf)) {

            $this->inscricao = $this->inscricaoDAO->loadPorCPF(InscricaoDAO::loadUserSession()->cpf);
        }

        if (isset($_GET['id'])) {
            $id = filter_input(INPUT_GET, 'id');

            $this->inscricao = $this->inscricaoDAO->load($id);
        }
    }

    public function index() {

        if ($this->usuarioLogado->others->portaria) {
            header('Location:' . _src('/encaminhado'));
        }


        $msg = filter_input(INPUT_GET, 'msg');

        if ($msg == 'vista_sucesso') {

            $this->mensagemSucessoVisita = true;
        } elseif ($msg == 'checkout') {
            $this->mensagemSucessoCheckout = true;
        } else {
            $this->mensagemSucessoVisita = false;
        }

        if ($this->inscricao->status_cadastro == Inscricao::STATUS_CADASTRO_CONCLUIDO) {
            $this->visitas = $this->visitaDAO->lista($this->inscricao->idinscricao);
        } else {
            $this->visitas = $this->visitaDAO->listaDia();
        }




        dispatcher();
    }

    public function close() {

        InscricaoDAO::logout();
        $this->index();
    }

    public function vista_detalhes() {

        $id = filter_input(INPUT_GET, 'cod', FILTER_VALIDATE_INT);

        $this->visita = $this->visitaDAO->load($id);


        dispatcher();
    }

    public function visitas() {

        $this->visitas = $this->visitaDAO->lista();



        dispatcher();
    }

    public function encaminhado() {

        $this->visitas = $this->visitaDAO->listaPorSetor($this->usuarioLogado->others->idsetor);

        dispatcher();
    }

    public function verifica() {

        $json = [];

        $json['ok'] = true;

        $this->visitas = $this->visitaDAO->listaPorSetorPendente($this->usuarioLogado->others->idsetor);


        $this->visitaDAO->finalizar($this->usuarioLogado->others->idsetor);

        $json['visitas'] = $this->visitas;

        echo json_encode($json);
    }

    public function nova_visita() {
        InscricaoDAO::logout();
        dispatcher();
    }

    public function registro() {



        $inscricaoTeste = $this->inscricaoDAO->loadPorCPF($this->inscricao->cpf);





        if ($inscricaoTeste) {

            $this->inscricao = $inscricaoTeste;
            InscricaoDAO::saveUserSession($this->inscricao);

            header('Location: ' . _src('/'));
        } else {
            InscricaoDAO::logout();
            header('Location: ' . _src('/inscricao?inscricao->cpf=' . $this->inscricao->cpf));
        }










        dispatcher();
    }

    public function inscricao() {



        $this->povoaAnexos();


        $this->listaNacionalidade = $this->metaDados->listaPorCodigo(Metadados::NACIONALIDADE);


        $this->listaEstados = DAO::lista('Estado');

        $this->listaCidades = DAO::lista('Cidade');



        dispatcher();
    }

    public function visita() {

        $this->listaSetores = $this->setorDAO->lista();


        #$print_pre($this->listaSetores);





        dispatcher();
    }

    public function __visitaDO() {

        $this->inscricao = InscricaoDAO::loadUserSession();
    }

    public function visitaDO() {

        $this->visita->inscricao = $this->inscricao;
        DAO::merge($this->visita);

        InscricaoDAO::logout();

        header('Location: ' . _src('/?msg=vista_sucesso'));
    }

    public function __inscricaoDO() {

        $this->inscricao = InscricaoDAO::loadUserSession();
    }

    public function inscricaoDO() {


        try {




            $arquivos = []; //rearrange($_FILES['anexos']);


            $this->validarCampos([
                $this->inscricao->nome,
                $this->inscricao->cargo,
                $this->inscricao->sexo,
                $this->inscricao->tel1,
                $this->inscricao->email
            ]);



            $path = Inscricao::CAMINHO_DOC . $this->inscricao->cpf . '/';




            $bollMkdir = is_dir(_file(Inscricao::CAMINHO_DOC . $this->inscricao->cpf)) || mkdir(_file(Inscricao::CAMINHO_DOC . $this->inscricao->cpf));
            if (!$bollMkdir) {
                throw new Exception('Não foi possível criar o diretório');
            }

            //salvando foto da webcam
            $imgWebCam = $_POST['hidden_data'];
            if (!empty($imgWebCam)) {
                $imgWebCam = str_replace('data:image/png;base64,', '', $imgWebCam);
                $imgWebCam = str_replace(' ', '+', $imgWebCam);
                $dataWebCam = base64_decode($imgWebCam);
                $fileWebCam = $path . 'foto_' . uniqid(rand(), true) . ".png";
                $WebCamsuccess = file_put_contents(_file($fileWebCam), $dataWebCam);
            }
            //end salvando foto da webcam



            foreach ($arquivos as $key => $item) {

                if (!empty($item['tmp_name'])) {

                    $tipoArquivo = strtolower(getTypeFile($item));
                    $nome = $key . '__' . uniqid(rand(), true) . '.' . $tipoArquivo;
                    #$nome = $key . '.' . $tipoArquivo;

                    $move = move_uploaded_file($item['tmp_name'], _file($path) . $nome);

                    if ($move == false) {
                        throw new Exception('Não foi possível fazer  o upload dos arquivos');
                    }

                    #$this->inscricao->$key = Inscricao::CAMINHO_DOC . $this->inscricao->cpf . '/' . $nome;
                }
            }

            if ($handle = opendir(_file($path))) {

                $files = [];
                while (false !== ($entry = readdir($handle))) {
                    $files[] = $entry;
                }

                foreach ($this->anexosObrigatorios as $obr) {


                    $encontou = false;
                    foreach ($files as $entry) {



                        if (strpos($entry, $obr) === 0) {

                            $this->anexos[] = $entry;
                            $encontou = true;
                            break;
                        }
                    }

                    if (!$encontou) {

                        throw new Exception('Anexo não localizado:' . $obr);
                    }
                }
            } else {

                throw new Exception('Não foi possível ler o diretório');
            }
            closedir($handle);

            // if ($this->inscricao->idinscricao) {
            //     $this->inscricao = DAO::load('Inscricao', $this->inscricao->idinscricao);
            // }


            $this->inscricao->status_cadastro = Inscricao::STATUS_CADASTRO_CONCLUIDO;

            if (!empty($fileWebCam)) {

                $this->inscricao->foto = _src($fileWebCam);
            }

            $send = empty($this->inscricao->idinscricao) ? true : false;

            $resultMerge = DAO::merge($this->inscricao);





            $this->inscricao = $this->inscricaoDAO->load($resultMerge->idinscricao);



            if (!empty($this->inscricao)) {

                InscricaoDAO::saveUserSession($this->inscricao);
                $_SESSION['inscricao'] = $this->inscricao;
                $_SESSION['id_inscricao'] = $this->inscricao->idinscricao;

                if ($send) {

                    header('Location: ' . _src('inscricao_sucesso?send=true'));
                } else {

                    header('Location: ' . _src('inscricao_sucesso'));
                }
            } else {

                throw new Exception('Não foi possível salvar os dados');
            }

            dispatcher();
        } catch (Exception $e) {

            logger(ERROR, $e->getMessage(), 'ERRO_INSCRICAO_');

            header('Location: ' . _src('inscricao_erro?erro=' . $e->getMessage()));
        }
    }

    public function sucesso() {

        if (InscricaoDAO::isUserLogged()) {

            $id = InscricaoDAO::loadUserSession()->idinscricao;

            $this->inscricao = $this->inscricaoDAO->load($id);

            $this->povoaAnexos();


            dispatcher();
        } else {
            header('Location: ' . _src('inscricao_erro?e=1'));
        }
    }

    public function excluirAnexo() {

        $json = [];


        $name = $_POST['doc'];

        $this->path = _file(Inscricao::CAMINHO_DOC . InscricaoDAO::loadUserSession()->cpf . '/');

        rename($this->path . '/' . $name, $this->path . '/' . 'excluido__' . $name);

        $json['ok'] = true;

        echo json_encode($json);
    }

    private function povoaAnexos($cpf = false) {

        if ($cpf) {


            $this->path = _file(Inscricao::CAMINHO_DOC . $cpf . '/');
        } else {

            $this->path = _file(Inscricao::CAMINHO_DOC . InscricaoDAO::loadUserSession()->cpf . '/');
        }



        if ($handle = opendir($this->path)) {

            while (false !== ($entry = readdir($handle))) {

                if ($entry != "." && $entry != ".." && strpos($entry, 'excluido') !== 0) {

                    $this->anexos[] = $entry;
                }
            }

            sort($this->anexos);

            closedir($handle);
        } else {
            InscricaoDAO::logout();
            exit('Não foi possivel obter os anexos');
        }
    }

    private function validarCampos($array) {

        foreach ($array as $a) {




            if (empty($a) && $a !== '0') {

                throw new Exception('Dados inconsistentes : ');
            }
        }
    }

    public function error() {

        unset($_SESSION['inscricao']);

        dispatcher();
    }

    public function cadastro() {


        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) | filter_input(INPUT_POST, 'inscricao->idinscricao', FILTER_VALIDATE_INT);

        if ($id) {

            $this->inscricao = $this->inscricaoDAO->load($id);
        }

        dispatcher();
    }

    public function __save() {

        $idinscricao = filter_input(INPUT_POST, 'inscricao->idinscricao', FILTER_VALIDATE_INT);

        if ($idinscricao) {

            $this->inscricao = $this->inscricaoDAO->load($idinscricao);
        }
    }

    public function save() {


        DAO::merge($this->inscricao);

        $this->mensagemSucesso = MSG_NOTIFICACAO_SALVO_SUCESSSO;

        $this->cadastro();
    }

    public function pesquisa() {


        $this->listaInscricao = $this->inscricaoDAO->pesquisa($this->inscricao);


        dispatcher();
    }

    public function pesquisaDO() {

        $this->pesquisa();
    }

    public function excluir() {

        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

        if ($id) {

            $inscricao = DAO::load('Inscricao', $id);

            $novo = (uniqid(rand(), true));
            $old = $inscricao->cpf;





            $inscricao->cpf = $old . '_' . $novo;
            $inscricao->stdelete = 0;

            $c = rename(_file(Inscricao::CAMINHO_DOC . $old), _file(Inscricao::CAMINHO_DOC . $inscricao->cpf));

            # echo $c;

            DAO::merge($inscricao, ['stdelete', 'cpf']);

            header('Location: ' . _src('/admin/inscritos.php?msg=') . ALERT_EXCLUIR_SUCESSO);
        } else {

            header('Location: ' . _src('/admin/detalhes.php?id=') . $inscricao->idinscricao . "&msg=" . ALERT_ERROR);
        }
    }

    public function detalhes() {


        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

        if ($id) {

            $this->inscrito = $this->inscricaoDAO->load($id);

            $this->povoaAnexos($this->inscrito->cpf);
        }




        dispatcher();
    }

    public function enviarEmailConfirmacaoAjax() {


        $id = InscricaoDAO::loadUserSession()->idinscricao;

        $inscricao = $this->inscricaoDAO->load($id);

        // Inicia a classe PHPMailer
        $mail = new PHPMailer();

        ob_start();
        #extract(get_object_vars($this->inscricao));
        include _file('/pages/notificacao_email.php');
        $mensagem = ob_get_contents();
        ob_end_clean();


        // Define os dados do servidor e tipo de conexão
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        $mail->Host = "ssl://smtp.gmail.com"; // Endereço do servidor SMTP
        $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
        $mail->Username = 'naoresponda.ctra@gmail.com'; // Usuário do servidor SMTP
        $mail->Password = 'casarao@ctra18'; // Senha do servidor SMTP
        $mail->Port = 465;
        $mail->SMTPSecure = 'tls';

        // Define o remetente
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->From = "suporte.secti.ma@gmail.com"; // Seu e-mail
        $mail->FromName = "Casarão Tech"; // Seu nome
        // Define os destinatário(s)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->AddAddress($inscricao->email, 'Casarão Tech');

        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
        // Define a mensagem (Texto e Assunto)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->Subject = "Casarão Tech"; // Assunto da mensagem
        $mail->Body = $mensagem;
        $mail->AltBody = $mensagem;

        $enviado = $mail->Send();

        // Limpa os destinatários e os anexos
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        if ($enviado) {
            $json['result'] = true;
        } else {
            $json['result'] = false;
        }

        echo json_encode($json);
    }

    public function comprovante_inscricao() {


        if (isset($_SESSION['id_inscricao'])) {

            $id = $_SESSION['id_inscricao'];


            $this->inscricao = $this->inscricaoDAO->load($id);
        }

        dispatcher();
    }

    public function relatorio_comprovante_inscricao() {

        $idinscricao = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

        if ($idinscricao) {

            $this->inscricao = $this->inscricaoDAO->load($idinscricao);

            $listaCidades = DAO::lista('Cidade', false, 'nome');
            $listaInstituicoes = DAO::lista('Instituicao', false, 'nome');

            foreach ($listaCidades as $item) {

                $this->cidades[$item->idcidade] = $item->nome;
            }

            foreach ($listaInstituicoes as $item) {

                $this->instituicoes[$item->idinstituicao] = $item->nome;
            }
        }

        dispatcher();
    }

    public function enviarEmailConfirmacao($id) {

        $this->inscricao = $this->inscricaoDAO->load($id);

        // Inicia a classe PHPMailer
        $mail = new PHPMailer();

        ob_start();
        extract(get_object_vars($this->inscricao));
        include _file('/pages/inscricao/notificacao_email.php');
        $mensagem = ob_get_contents();
        ob_end_clean();

        // Define os dados do servidor e tipo de conexão
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        $mail->Host = "ssl://smtp.gmail.com"; // Endereço do servidor SMTP
        $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
        $mail->Username = 'suporte.secti.ma@gmail.com'; // Usuário do servidor SMTP
        $mail->Password = 'druk#meXE3+R'; // Senha do servidor SMTP
        $mail->Port = 465;
        $mail->SMTPSecure = 'tls';

        // Define o remetente
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->From = "suporte.secti.ma@gmail.com"; // Seu e-mail
        $mail->FromName = " Programa Transporte Universitário"; // Seu nome
        // Define os destinatário(s)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->AddAddress($this->inscricao->email, 'Inscricao Programa Transporte Universitário');

        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
        // Define a mensagem (Texto e Assunto)
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->Subject = "Inscricao Programa Transporte Universitário"; // Assunto da mensagem
        $mail->Body = $mensagem;
        $mail->AltBody = $mensagem;

        $enviado = $mail->Send();

        // Limpa os destinatários e os anexos
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        return $enviado;
    }

    public function delete() {
        $this->visita = DAO::load('Visita', $_GET['id']);
        $this->visita->stdelete = 0;

        DAO::merge($this->visita, ['stdelete']);
        $this->mensagemSucesso = MSG_NOTIFICACAO_EXCLUIR_SUCESSSO;

        $this->visitas();
    }

    public function deleteCadastro() {
        $this->inscricao = DAO::load('Inscricao', $_GET['id']);

        $this->inscricao->stdelete = 0;

        DAO::merge($this->inscricao, ['stdelete']);
        $this->mensagemSucesso = MSG_NOTIFICACAO_EXCLUIR_SUCESSSO;

        $this->inscricoes();
    }

    public function checkout() {
        $visita = DAO::load('Visita', $_GET['visita']);

        $visita->checkout = date('Y-m-d H:i:s');

        DAO::merge($visita, ['checkout']);

        $this->mensagemSucesso = 'Checkout realizado com sucesso';

        header('Location: ' . _src('/?msg=checkout'));
    }

    public function relatorios() {
        $this->listaSetores = $this->setorDAO->lista();

        dispatcher();
    }

    public function relatorioSalas() {
        list($dataInicio, $dataFim) = explode('-', trim(filter_input(INPUT_GET, 'de_ate')));

        $sala = filter_input(INPUT_GET, 'sala');

        $this->data = $this->visitaDAO->loadRelatorioVisitas($dataInicio, $dataFim, $sala);
        $this->dataInicio = $dataInicio;
        $this->dataFim = $dataFim;

        dispatcher();
    }

    public function inscricoes() {
        $this->inscritos = DAO::lista('Inscricao', ['stdelete' => 1]);

        dispatcher();
    }

}

Controller_execute("InscricaoCtrl");

require_once (_file('/lib/myphp/dispatcher.inc.php'));
