<?php

require_once __DIR__ . '/../myconfig.php';

/**
 *
 *
 * @author Geanderson
 */
class RelatorioCtrl extends Controllers {

    public $typefile;
    public $exportacao = [];
  

    public function __construct() {

        $this->alunoDAO = new AlunoDAO;
    
        $date = new DateTime();
        $this->dataRodape = $date->format('d/m/Y     H:i');

        $this->typefile = 'pdf';

  
        parent::__construct();
    }

    public function index(){

        dispatcher();
    
    }


    public function alunos_estagio() {

        if (!isset($this->exportacao[0])) {
            $this->exportacao = ['nome', 'cpf', 'data_inicio', 'data_fim'];
        }

        $date_anterior_mes = new DateTime('+1 Month');
        $date_anterior_semana = new DateTime('+1 Week');

        $this->date_now = new DateTime();

        // Formatando datas
        $dataMes = $date_anterior_mes->format('Y/m/d');
        $dataSemana = $date_anterior_semana->format('Y/m/d');

        if($this->tipo_relatorio == "mes"){
       
            $this->listaAluno = $this->alunoDAO->listaDataFimEstagioMes($dataMes);
       
        }elseif($this->tipo_relatorio == "semana"){
       
            $this->listaAluno = $this->alunoDAO->listaDataFimEstagioSemana($dataSemana);
       
        }


       
        dispatcher();
    }

 
}

Controller_execute("RelatorioCtrl");

require_once (_file('/lib/myphp/dispatcher.inc.php'));
