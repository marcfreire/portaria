-- Column: nis_localizado

-- ALTER TABLE public.inscricao DROP COLUMN nis_localizado;

ALTER TABLE public.inscricao ADD COLUMN nis_localizado character varying;

-- Column: nis_valido

-- ALTER TABLE public.inscricao DROP COLUMN nis_valido;

ALTER TABLE public.inscricao ADD COLUMN nis_valido character varying;

-- Column: nis_nome

-- ALTER TABLE public.inscricao DROP COLUMN nis_nome;

ALTER TABLE public.inscricao ADD COLUMN nis_nome character varying;


-- Column: nis_cpf

-- ALTER TABLE public.inscricao DROP COLUMN nis_cpf;

ALTER TABLE public.inscricao ADD COLUMN nis_cpf character varying;

-- Column: nis_rg

-- ALTER TABLE public.inscricao DROP COLUMN nis_rg;

ALTER TABLE public.inscricao ADD COLUMN nis_rg character varying;


