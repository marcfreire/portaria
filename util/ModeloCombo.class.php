<?php

require_once __DIR__ . '/../myconfig.php';

class ModeloCombo {

    public static $boxStatusInscricao = [
        Inscricao::STATUS_PENDENTE => 'PENDENTE',
        Inscricao::STATUS_CLASSIFICADO => 'CLASSIFICADO',
        Inscricao::STATUS_DESCLASSIFICADO => 'DESCLASSIFICADO',
    ];
    public static $boxSexo = [
        1 => 'Masculino',
        2 => 'Feminino',
    ];
    public static $boxSimNao = [
        1 => 'Sim',
        2 => 'Não',
    ];
    public static $boxFiltroAluno = [
        'nome' => 'Nome',
        'cpf' => 'CPF',
        'matricula' => 'Mátricula',
        'data_inicio' => 'Data de Início',
        'data_fim' => 'Data de Fim',
        'empresa' => 'Empresa',
        'tipo' => 'Tipo do Aluno',
        'tipo_estagio' => 'Tipo do Estágio',
        'seguro' => 'Seguro',
    ];
    public static $boxTipoCota = [
        Aluno::COTA_AMPLA => 'Ampla concorrência',
        Aluno::COTA_PUBLICA => 'Cota para escola pública',
        Aluno::COTA_ESPECIAL => 'Cota para deficientes',
      
    ];

}
