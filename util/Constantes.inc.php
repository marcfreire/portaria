<?php



define("ALERT_SENHA_ALTERADA_ERRO", 'Senha incorreta');

define("MSG_NOTIFICACAO_SALVO_SUCESSSO", 'Salvo com sucesso');
define("MSG_NOTIFICACAO_EXCLUIR_SUCESSSO", 'Excluído com sucesso');
define("MSG_NOTIFICACAO_ALUNO_EMPRESA_ERRO", 'A empresa está vinculada a um ou mais alunos');
define("MSG_EMAIL_CONTATO_SUCESSO", 'Mensagem enviada com sucesso');
define("MSG_EMAIL_CONTATO_ERROR", 'Erro ao enviar mensagem, tente novamente!');



define('FOOTER_FONT_SIZE', 7);
define('FOOTER_TEXT', 'Emitido pelo sistema de  Inscrição IEMA às ' . date("H:i:s") . ' h do dia ' . date("d/m/Y"));


define("ALERT_CLASSIFICADO_SUCESSO", 'w34gf');
define("ALERT_DESCLASSIFICADO_SUCESSO", 's564gf');
define("ALERT_EXCLUIR_SUCESSO", 'we58c');
define("ALERT_ERROR", 'dr453');

$msgAlerts = [
    ALERT_CLASSIFICADO_SUCESSO => ['O inscrito foi classificado com sucesso'],
    ALERT_DESCLASSIFICADO_SUCESSO => ['O inscrito foi desclassificado com sucesso'],
    ALERT_EXCLUIR_SUCESSO => ['O inscrito foi excluído com sucesso'],
    ALERT_ERROR => ['Erro ao executar a ação, tente novamente'],
   
 
];

define("NOTIFY_EXCLUIR_SUCESSSO", '466c5dw');

$msgNotify=[
    
    NOTIFY_EXCLUIR_SUCESSSO=> [],
   
];


class Constantes {
    //put your code here
}
