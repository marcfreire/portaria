<?php

date_default_timezone_set('America/Sao_Paulo');
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
session_start();
require_once __DIR__ . '/../myconfig.php';
require_once (_file('/util/Constantes.inc.php'));

require_once (_file('/vendor/autoload.php'));


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;



function phpMailer_send($options) {
    
    $conf_host = "ssl://smtp.gmail.com";
    $conf_user = 'suporte.secti.ma@gmail.com';
    $conf_password = 'druk#meXE3+R';


    // Inicia a classe PHPMailer
    $mail = new PHPMailer();

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP
    $mail->Host = $conf_host; // Endereço do servidor SMTP
    $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
    $mail->Username = $conf_user; // Usuário do servidor SMTP
    $mail->Password = $conf_password; // Senha do servidor SMTP
    $mail->Port = 465;
    $mail->SMTPSecure = 'tls';

    // Define o remetente
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->From = $conf_user; // Seu e-mail
    $mail->FromName = $options['name']; // Seu nome
    // Define os destinatário(s)
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->AddAddress($options['address']);

    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
    $mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
    // Define a mensagem (Texto e Assunto)
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->Subject = $options['subject']; // Assunto da mensagem
    $mail->Body = $options['message'];
    $mail->AltBody =  $options['message'];


    $send = $mail->Send();

    // Limpa os destinatários e os anexos
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
    
    return $send;
}