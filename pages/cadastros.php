<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('inscricoes');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <? require_once _file('/pages/part/part_head.php') ?>
</head>
<body>
        <div class="page home-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch">
                <!-- Side Navbar -->
                <nav class="side-navbar">      
                    <? require_once _file('/pages/part/part_nav.php') ?>         
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <!-- Dashboard Counts Section-->        

                    <!-- Projects Section-->
                    <section class="projects">
                        <div class="container-fluid">
                            <div class="card">
                                <div class="card-header d-flex align-items-center">
                                    <h3 class="h4">
                                        <i class="fa fa-list" aria-hidden="true"></i> 
                                        Cadastros
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <table id="table_id" class="display">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>CPF</th>
                                                <th>Dt. cadastro</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?foreach($inscritos as $i):?>
                                            <tr>
                                                <td><?=$i->nome ?></td>
                                                <td><?=$i->cpf ?></td>
                                                <td><?=fixDate($i->datacadastro)?></td>
                                                <td>
                                                    <a href="<?=_src('inscricao?id='.$i->idinscricao)?>" class="btn btn-info btn-sm "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn btn-sm btn-danger excluir" data-id="<?=$i->idinscricao ?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                           <?endforeach?>
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </section>




                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>

    </body>

    <script>
        $(document).ready(function () {
            $('#table_id').DataTable();

            $('.excluir').click(function(){              
            var id = $(this).attr('data-id');

            alertify
                .confirm(
                    'Cadastro',
                    'Excluir?',
                    function(){ excluir(id); },
                    function(){}
                )
            
            });

            function excluir(id){
                window.location.href = '<?=_src('/cadastros?go=deleteCadastro&id=')?>'+id        
            }
        });
    </script>
</html>