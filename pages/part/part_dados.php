<h4 class="mb-5">
    Seja bem-vindo (a) ao Casarão Tech Renato Archer
</h4>
<p>
    Este é o seu comprovante de cadastro em nosso sistema. 
</p>
<p>
    Fique por dentro de nossas atividades. Siga nossas redes sociais!
</p>
<p>
    Facebook: <a target="_blank" href="https://www.facebook.com/casaraotech/">Casarão Tech</a>
</p>
<p>
    Instagram: <a target="_blank" href="https://www.instagram.com/casaraotech/">casaraotech</a>
</p>
<p>
    Site: <a target="_blank" href="http://ctra.secti.ma.gov.br">www.ctra.secti.ma.gov.br</a>
</p>
<p>
    Saiba também, sobre tudo o que rola no sistema de Ciência e Tecnologia do Maranhão:
</p>
<p>
    Facebook: <a target="_blank" href="https://www.facebook.com/sectima">SECTI Maranhão</a>
</p>
<p>
    Instagram: <a target="_blank" href="https://www.instagram.com/sectimaranhao">@sectimaranhao</a>
</p>
<p>
    Site: <a target="_blank" href="www.secti.ma.gov.br">www.secti.ma.gov.br</a>
</p>