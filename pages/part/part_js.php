<!-- Javascript files-->

<script src="<?=_src('/node_modules/jquery/dist/jquery.min.js')?>"></script>
<script src="<?=_src('/pages/js/tether.min.js')?>"></script>
<script src="<?=_src('/pages/js/bootstrap.min.js')?>"></script>



<script src="<?=_src('/node_modules/alertifyjs/build/alertify.min.js')?>"></script>
<script src="<?=_src('/node_modules/jquery-validation/dist/jquery.validate.min.js')?>"></script>

<script src="<?=_src('/node_modules/datatables.net/js/jquery.dataTables.js')?>"></script>


<script src="<?=_src('/pages/js/validaocao_mascara.js?2')?>"></script>
<script src="<?=_src('/pages/js/front.js')?>"></script>
<script src="<?=_src('/pages/js/jquery.mask.min.js')?>"></script>
<script src="<?=_src('/pages/js/notificacao.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    
        
    
        <? if (isset($mensagemSucesso) && !empty($mensagemSucesso)): ?>
            alertify.notify('<?=$mensagemSucesso?>', 'success', 5, function(){   });
        <? endif; ?>
        <? if (isset($mensagemErro) && !empty($mensagemErro)): ?>
          
            alertify.notify('<?=$mensagemErro?>', 'error', 5, function(){   });
        <? endif; ?>
</script>
