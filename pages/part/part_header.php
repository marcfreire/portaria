<nav class="navbar">
    <!-- Search Box-->
    
    <div class="container-fluid">
        <div class="navbar-holder d-flex align-items-center justify-content-between">
            <!-- Navbar Header-->
            <div class="navbar-header">
                <!-- Navbar Brand --><a href="<?= _src('/') ?>" class="navbar-brand">
                  
                    <div class="brand-text brand-big hidden-lg-down"><span> </span><strong>  <img src="<?= _src('/pages/img/CTRA_Prancheta 1_Prancheta 1.png') ?>" class="img-responsive" style="max-width: 100%;max-height: 50px"></strong></div>
                    <div class="brand-text brand-small"><strong></strong></div></a>
                <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
            </div>
            <!-- Navbar Menu -->
            <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">


                <!-- Logout    -->
                <li class="nav-item"><a href="<?= _src('/login?go=logout') ?>" class="nav-link logout">Sair<i class="fa fa-sign-out"></i></a></li>
            </ul>
        </div>
    </div>
</nav>