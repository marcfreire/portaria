<!-- Sidebar Header-->
<div class="sidebar-header d-flex align-items-center">
    <div class="">
        
      
     
     <?if(!empty($usuarioLogado->others->foto)):?>  
       
        <img style="width: 100px;height:100px;border-radius: 50%;display: inline-block" src="<?=_src('upload/fotos/'.$usuarioLogado->others->foto)?>">
        
     <?else:?>
        
         
        
     <?endif?>
    
    </div>
    <div class="title">
        <h1 style="display: inline-block" class="h4"><?= $usuarioLogado->nome ?></h1>

    </div>
    
</div> 

<?if($usuarioLogado->others->portaria):?>


<ul class="list-unstyled">
    
    
    <li <?if(getNameScript()=='encaminhado'):?>class="active"<?endif?>  > <a href="<?=_src('encaminhado')?>"><i class="fa fa-home" aria-hidden="true"></i> Encamihado</a></li>
   
    
</ul>

<?else:?>

<a  style="border-radius: 0;box-shadow: 0px 17px 10px -10px!important "  class="btn btn-lg btn-success mb-5 btn-block" href="<?=_src('/nova_visita')?>"><i class="fa fa-plus-square" aria-hidden="true"></i> <span style="color: #fff">CHECKIN</span></a>
                            


<ul class="list-unstyled">
    
    
    <li <?if(getNameScript()=='index'):?>class="active"<?endif?>  > <a href="./"><i class="fa fa-home" aria-hidden="true"></i> Início</a></li>
    
    <li <?if(getNameScript()=='cadastros'):?>class="active"<?endif?>  > <a href="<?=_src('/cadastros')?>"><i class="fa fa-archive" aria-hidden="true"></i> Cadastros</a></li>
    <li <?if(getNameScript()=='visitas'):?>class="active"<?endif?>  > <a href="<?=_src('/visitas')?>"><i class="fa fa-list" aria-hidden="true"></i> Visitas</a></li>
    <li <?if(getNameScript()=='setores'):?>class="active"<?endif?>  > <a href="<?=_src('/setores')?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i> Salas</a></li>
    <li <?if(getNameScript()=='relatorios'):?>class="active"<?endif?>  > <a href="<?=_src('/relatorios')?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Relatórios</a></li>
    <li <?if(getNameScript()=='usuarios'):?>class="active"<?endif?>  > <a href="<?=_src('/usuarios')?>"><i class="fa fa-users" aria-hidden="true"></i></i> Usuários</a></li>
    <li <?if(getNameScript()=='alterar_senha'):?>class="active"<?endif?>  > <a href="<?=_src('/alterar_senha')?>"><i class="fa fa-lock" aria-hidden="true"></i></i> Alterar senha</a></li>
  
</ul>


<?endif?>
