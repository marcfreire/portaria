<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Casarão Tech</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<!-- Bootstrap CSS-->
<link rel="stylesheet" href="<?=_src('/pages/css/bootstrap.css')?>">
<!-- Google fonts - Roboto -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
<!-- theme stylesheet-->
<link rel="stylesheet" href="<?=_src('/pages/css/style.blue.css?1')?>" id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="<?=_src('/pages/css/custom.css')?>">
<!-- Favicon-->
<link rel="shortcut icon" href="img/favicon.ico">
<!-- Font Awesome CDN-->
<!-- you can replace it by local Font Awesome-->
<link rel="stylesheet" href="<?=_src('/pages/css/font-awesome.min.css')?>">

<!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<link rel="stylesheet" href="<?=_src('/node_modules/alertifyjs/build/css/alertify.min.css')?>">
<link rel="stylesheet" href="<?=_src('/node_modules/alertifyjs/build/css/themes/default.min.css')?>">



<link rel="stylesheet" href="<?=_src('/node_modules/datatables.net-dt/css/jquery.dataTables.css')?>">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />