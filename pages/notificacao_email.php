<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('sucesso');

require_once (_file('/controllers/InscricaoCtrl.php'));


?>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>IEMA</title>
        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">



        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="<?= _src('/pages/css/bootstrap.css') ?>">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="<?= _src('/pages/css/style.green.css') ?>" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="email.css">


    </head>
    <body>

            <div class="container">


                <div class="row">
                    <div class="col-lg-12">  
                        <img  style="max-width: 400px" src="https://atendimentoctra.secti.ma.gov.br/pages/img/vindo.jpg">
                    </div>
                </div>
                
                <div class="row pt-5">
                    
                    

                    <!-- Basic Form-->
                    <div class="col-lg-12">
                        <div class="card">

                            
                            
                            <div class="card-header d-flex align-items-center">
                                <h3 class="h4">CTRA </h3>
                            </div>
                            <div class="card-body">

                                <div class="pt-5">
                                    <? require_once _file('/pages/part/part_dados.php') ?>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


     



    </body>
</html>

