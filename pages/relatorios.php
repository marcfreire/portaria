<?
require_once __DIR__ . '/../myconfig.php';


require_once __DIR__ . '/social.inc.php';

Controller_defaultMethod('relatorios');

require_once (_file('/controllers/InscricaoCtrl.php'));


?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <style>
            .select2-choices {
  min-height: 150px;
  max-height: 150px;
  overflow-y: auto;
}

        </style>
        
    </head>
    <body>
        <div class="page form-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
                <!-- Side Navbar -->
                <nav class="side-navbar">
                    <? require_once _file('/pages/part/part_nav.php') ?>
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Gerar relatório</h2>
                        </div>
                    </header>
                    <ul class="breadcrumb">
                        <div class="container-fluid">
                            <li class="breadcrumb-item"><a href="<?= _src('/') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Relatórios</li>
                        </div>
                    </ul>
                    <!-- Forms Section-->
                    <section class="forms"> 
                        <div class="container-fluid">
                            <div class="row">
                                <!-- Basic Form-->
                                <div class="col-lg-12">
                                    
                                    <form  method="GET"  action="<?=_src('/pages/relatorio_visitas.php')?>" enctype="multipart/form-data"   class="jquery-validation" id="form1">

                                        
                                        
                                        
                                        <div class="card">

                                            <div class="card-header d-flex align-items-center">
                                                <h3 class="h4"><i class="fa fa-plus" aria-hidden="true"></i> Relatório</h3>
                                            </div>
                                            
                                            
                                            <div class="card-body">

                                              

                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label for="de_ate" class="label-control d-block">De / Até*</label>
                                                        <input type="text" name="de_ate" id="datepicker" class="form-control" required>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" for="visita_setor_idsetor">Sala*</label>
                                                        <select name="sala" class="form-control js-example-basic-single"  >
                                                            <option  value=''>&nbsp</option> 
                                                            <? foreach ($listaSetores as $item) : ?>
                                                            
                                                                
                                                                   
                                                                        <option  <? @select($item->idsetor, $visita->setor->idsetor) ?> value='<?= $item->idsetor ?>'><?= ($item->nome) ?></option> 
                                                                   
                                                                
                                                                <? endforeach; ?>
                                                        </select>
                                                    </div>
                                                    


                                                </div>    

                                              
                                                  

                                               

                                            </div>
                                        </div>
                                      
                                         
                                        
                                         
                                        <div class="row">
                                            <div class="form-group col-md-12 text-center pt-5">  
                                                <input type="submit" value="Gerar relatório" class="btn btn-primary  btn-lg">
                                            </div>
                                        </div>
                                        
                                       
                                    </form>

                                </div>
                            </div>
                    </section>

                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script>
            
            
            $(function(){
                
                  $('.js-example-basic-single').select2({
                      
                     theme: "classic"
                  });

                  $('#datepicker').daterangepicker({
                    autoApply: true,
                    locale: {
                        format: 'DD/MM/YYYY',
                        monthNames: [
                            "Janeiro",
                            "Fevereiro",
                            "Março",
                            "Abril",
                            "Maio",
                            "Junho",
                            "Julho",
                            "Augosto",
                            "Setembro",
                            "Outubro",
                            "Novembro",
                            "Dezembro"
                        ],
                        daysOfWeek: [
                            "Dom",
                            "Seg",
                            "Ter",
                            "Qua",
                            "Qui",
                            "Sex",
                            "Sab"
                        ]
                    }
                  })
            });
            
       
        </script>
    </body>
</html>