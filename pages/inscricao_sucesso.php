<?
require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/social.inc.php';

Controller_defaultMethod('sucesso');

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page form-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
                <!-- Side Navbar -->
                <nav class="side-navbar">
                    <? require_once _file('/pages/part/part_nav.php') ?>
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech </h2>
                        </div>
                    </header>
                    <ul class="breadcrumb">
                        <div class="container-fluid">
                            <li class="breadcrumb-item"><a href="<?= _src('/') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Portaria</li>
                        </div>
                    </ul>
                    <!-- Forms Section-->
                    <section class="forms"> 
                        <div class="container-fluid">
                            
                            <div class="row">
                                <div class="col-md-12 text-center ">  
                                         <a  class="btn btn-success" href="<?=_src('/visita')?>"><i class="fa fa-plus" aria-hidden="true"></i> Registrar Visita</a>
                                    <a target="blank" class="btn btn-primary"  href="<?=_src('/comprovante')?>"><i class="fa fa-print" aria-hidden="true"></i> Imprimir comprovante</a>
                                    <a  class="btn btn-warning" href="<?=_src('/inscricao?id=' . $inscricao->idinscricao)?>"><i class="fa fa-pencil" aria-hidden="true"></i> Alterar dados</a>
                                   
                                </div>
                            </div>
                            
                            <div class="row pt-5">
                                
                                <!-- Basic Form-->
                                <div class="col-lg-12">
                                    <div class="card">
                                       
                                        <div class="card-header d-flex align-items-center">
                                            <h3 class="h4">Cadastro CTRA</h3>
                                        </div>
                                        <div class="card-body">
                                        <div class="alert alert-success"alert">


                                             <div>Cadastro Atualizado com Sucesso!</div>


                                        </div>
                                        <div class="pt-5">
                                            
                                            <img style="max-width: 400px" class="mb-5" src="<?=_src('/pages/img/vindo.jpg')?>">
                                            
                                                <? require_once _file('/pages/part/part_dados.php') ?>
                                            
                                        </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>
        
        
        
        <script>
            
            
            $(function(){
                
               <?if(isset($_GET['send'])):?> 
                    enviarEmail();
               <?endif?>
                
            })
            
            function enviarEmail(){
            
                    $.ajax({
                        type: "POST",
                        url: "<?= _url('/controllers/InscricaoCtrl.php?go=enviarEmailConfirmacaoAjax') ?>",
                        dataType:'json',
                        beforeSend: function () {

                        },
                        success: function (json) {
                       
                        },
                        error: function (txt,e) {
                           console.log(e);
                           alert('Erro');
                         
                        }
                    });
            
            
            }
        </script>
    </body>
</html>
