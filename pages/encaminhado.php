<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('encaminhado');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page home-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch">
                <!-- Side Navbar -->
                <nav class="side-navbar">      
                    <? require_once _file('/pages/part/part_nav.php') ?>         
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <!-- Dashboard Counts Section-->        

                    <!-- Projects Section-->
                    <section class="projects">
                        <div class="container-fluid">  

                           
                           
                           
                               
                            
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4"><i class="fa fa-list" aria-hidden="true"></i> Visitantes</h3>
                                    </div>
                                    <div class="card-body">
                                        <table id="table_id" class="display">
                                            <thead>
                                                <tr>
                                                     <th>Nome</th>
                                                    <th>Data e Hora</th>
                                                    <th>Setor</th>
                                                     <th>Orgão</th>
                                                    <th>#</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <? foreach ($visitas as $v): ?>
                                                    <tr>
                                                        <td><?= $v->visita_nome ?></td>
                                                        <td><?= fixDate($v->datacadastro) ?></td>
                                                        <td><?= $v->setor_nome ?></td>
                                                         <td><?= $v->setor_mestre_nome ?></td>
                                                        <td>

                                                       

                                                            <a class="btn btn-success" href="<?=_src('/visita_detalhes?cod=').$v->idvisita?>">  <i class="fa fa-info" aria-hidden="true"></i></a>
                                                            <a class="btn btn-primary" href="<?=_src('/visita_detalhes?cod=').$v->idvisita?>">   <i class="fa fa-arrow-right" aria-hidden="true"></i></i></a>
                                                       
                                                       
                                                        </td>
                                                    </tr>
                                                <? endforeach ?>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            

                        </div>
                    </section>




                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>

    </body>
    <script>

        $(document).ready(function () {
            $('#table_id').DataTable();
            
            
            setInterval(function(){ verifica(); }, 3000);
            
        });
        
        
        function verifica(){
            
           $.ajax({
                url: "<?=_src('/controllers/InscricaoCtrl.php?go=verifica')?>",
                dataType:'json'
               
          })
          .done(function( data ) {
               console.log(data);
               if(data.ok==true){
                   
                   data.visitas.forEach(function(element, index, array){
                       
                       
                       notificacao(element.visita_nome,element.foto);
                   });
                   if(data.visitas.length>0){
                        location.reload();
                   }
                  
                   
               }
                
          }).fail(function(err) {
               console.log(err);
          });
        }
    </script>
</html>