<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('index');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page home-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch">
                <!-- Side Navbar -->
                <nav class="side-navbar">      
                    <? require_once _file('/pages/part/part_nav.php') ?>         
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <!-- Dashboard Counts Section-->        

                    <!-- Projects Section-->
                    <section class="projects">
                        <div class="container-fluid">  

                            <? if (isset($mensagemSucessoVisita) && $mensagemSucessoVisita): ?>
                                <div class="alert alert-success">
                                    Vista Registrada com Sucesso ás <?= date('H:i:s') ?> data <?= date('d/m/Y') ?> 

                                </div>
                            <? endif ?>

                            <? if (isset($mensagemSucessoCheckout) && $mensagemSucessoCheckout): ?>
                                <div class="alert alert-success">
                                    Checkout Registrado com Sucesso ás <?= date('H:i:s') ?> data <?= date('d/m/Y') ?> 

                                </div>
                            <? endif ?>
                            
                           
                            <? if (InscricaoDAO::isUserLogged()): ?>
                                <div class="project">
                                    <div class="row bg-white has-shadow">
                                        <div class="left-col col-lg-4 d-flex align-items-center justify-content-between">
                                            <div class="project-title d-flex align-items-center">
                                                <div class="icon">
                                                    
                                                    <?if(!empty($inscricao->foto)):?>
                                                        <img style="width: 106px;height: 80px" src="<?=($inscricao->foto)?>">
                                                    <?endif?>
                                                </div>
                                                <div class="text">
                                                    <h3 class="h4"><?= $inscricao->nome ?> </h3>
                                                    <h3 class="h5">CPF/RG: <?= $inscricao->cpf ?> </h3>
                                                   <h3 class="h5">Cargo: <?= $inscricao->cargo ?> </h3>
                                                </div>
                                            </div>

                                        </div>




                                        <div class="right-col col-lg-4 d-flex align-items-center">
                                            <a style="display: block; margin: 0 auto;"  class="btn btn-success" href="<?= _src('/visita') ?>"><i class="fa fa-plus" aria-hidden="true"></i> Registrar Visita</a>
                                        </div>

                                        <div class="right-col col-lg-2 d-flex align-items-center">
                                            <a  class="btn btn-sm btn-warning" href="<?= _src('/inscricao') ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Alterar dados</a>


                                        </div>
                                        <div class="right-col col-lg-2 d-flex align-items-center">
                                            <a  class="btn btn-sm btn-info" href="<?= _src('/?go=close') ?>"><i class="fa fa-close" aria-hidden="true"></i></a>


                                        </div>
                                         


                                    </div>
                                </div>
                                <!-- Project-->
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4"><i class="fa fa-list" aria-hidden="true"></i> Visitas Registradas de <?= $inscricao->nome ?></h3>
                                    </div>
                                    <div class="card-body">
                                        <table id="table_id" class="display">
                                            <thead>
                                                <tr>
                                                    <th>Data e Hora</th>
                                                    <th>Horário de saída</th>
                                                    <th>Sala</th>
                                                    
                                                    <th>#</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <? foreach ($visitas as $v): ?>
                                                    <tr>
                                                        <td><?= fixDate($v->datacadastro) ?></td>
                                                        <td>
                                                            <? if ($v->checkout): ?>
                                                                <?= fixDate($v->checkout) ?>
                                                            <? endif ?>
                                                        </td>
                                                        <td><?= $v->setor_nome ?></td>
                                                         
                                                        <td>

                                                            <a class="btn btn-success" href="<?=_src('/visita_detalhes?cod=').$v->idvisita?>">  <i class="fa fa-info" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                <? endforeach ?>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            <? else: ?>
                                <!-- Project-->
                                <div class="card">
                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4"><i class="fa fa-list" aria-hidden="true"></i> Visitas Registradas de hoje</h3>
                                    </div>
                                    <div class="card-body">
                                        <table id="table_id" class="display">
                                            <thead>
                                                <tr>
                                                    <th>Visitante</th>
                                                    <th>Data e Hora</th>
                                                    <th>Horário de saída</th>
                                                    <th>Sala</th>
                                                    
                                                    <th>#</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <? foreach ($visitas as $v): ?>
                                                    <tr>
                                                        <td><?= $v->visita_nome ?></td>
                                                        <td><?= fixDate($v->datacadastro) ?></td>
                                                        <td>
                                                            <? if ($v->checkout): ?>
                                                                <?= fixDate($v->checkout) ?>
                                                            <? endif ?>
                                                        </td>
                                                        <td><?= $v->setor_nome ?></td>
                                                         
                                                        <td <? if(!$v->checkout): ?>style="width: 20%" <? endif ?>>

                                                            <a class="btn btn-success" href="<?=_src('/visita_detalhes?cod=').$v->idvisita?>">  <i class="fa fa-info" aria-hidden="true"></i></a>
                                                            <? if(!$v->checkout): ?>
                                                            <a data-id="<?=$v->idvisita ?>" class="checkout btn btn-primary text-white" style="cursor: pointer;">
                                                                Checkout
                                                            </a>
                                                            <? endif ?>
                                                        </td>
                                                    </tr>
                                                <? endforeach ?>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                                
                            <?endif?>


                        </div>
                    </section>




                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>

    </body>
    <script>

        $(document).ready(function () {
            $('#table_id').DataTable({
                order: [[ 1, "desc" ]]
            });

            $('.checkout').click(function(){              
              var id = $(this).attr('data-id');

              alertify
                .confirm(
                    'Visita',
                    'Confirmar checkout?',
                    function(){ window.location.replace('<?=_src('?go=checkout&visita=') ?>' + id) },
                    function(){}
                )
             
            });
        });
    </script>
</html>