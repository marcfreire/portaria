<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('relatorioSalas');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));

ob_start();
?>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Portaria Casarão Tech</title>
        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">



        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="<?= _file('/pages/css/bootstrap.css') ?>">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="<?= _file('/pages/css/style.green.css') ?>" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="<?= _file('/pages/css/custom.css') ?>">

        <style>
            div > table.zebrada tbody tr:nth-child(2n+1) {
                background-color: #EFEFEF!important;
            }

            @media print{

                *{
                    -webkit-print-color-adjust:exact!important;
                }
                }
        </style>


    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Visitas no período de <?=$dataInicio ?> à <?=$dataFim ?></h1>
                    <table class="table table-bordered zebrada mt-5">
                        <thead>
                            <tr>
                                <th>Visitante</th>
                                <th>Horário de entrada</th>
                                <th>Horário de saída</th>
                                <th>Sala</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach($data as $item): ?>
                                <tr>
                                    <td><?=$item->nome_visitante ?></td>
                                    <td><?=fixDate($item->datacadastro) ?></td>
                                    <td><?=$item->checkout ? fixDate($item->checkout) : 'Não registrado' ?></td>
                                    <td><?=$item->nome_setor ?></td>
                                </tr>
                            <? endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>

<?
$content = ob_get_clean();

$pdf = new \mikehaertl\wkhtmlto\Pdf([
    'footer-font-size' => FOOTER_FONT_SIZE,
    'footer-center' => '[page]',
    'footer-right' => FOOTER_TEXT,
    'footer-spacing' => -4,
    'commandOptions' => [
        'procEnv' => [
            'LANG' => 'pt_BR.utf-8.',
        ],
    ],
        ]);

$pdf->addPage($content);
#echo $content;
$pdf->send();
