<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('sucesso');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));

ob_start();
?>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Portaria Casarão Tech </title>
        <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">



        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="<?= _file('/pages/css/bootstrap.css') ?>">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="<?= _file('/pages/css/style.green.css') ?>" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="<?= _file('/pages/css/custom.css') ?>">


    </head>
    <body>

            <div class="container">


                <div class="row">
                    <div class="col-lg-12">  
                        <img src="<?= _file('/pages/img/logo_transporte.png') ?>">
                    </div>
                </div>
                
                <div class="row pt-5">
                    
                    

                    <!-- Basic Form-->
                    <div class="col-lg-12">
                        <div class="card">

                            
                            <div class="card-body">

                                <div class="pt-5">
                                    <? require_once _file('/pages/part/part_dados.php') ?>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


     



    </body>
</html>

<?
$content = ob_get_clean();
$pdf = new \mikehaertl\wkhtmlto\Pdf([
    'footer-font-size' => FOOTER_FONT_SIZE,
    'footer-center' => '[page]',
    'footer-right' => FOOTER_TEXT,
    'footer-spacing' => -4,
    'commandOptions' => [
        'procEnv' => [
            'LANG' => 'pt_BR.utf-8.',
        ],
    ],
        ]);

$pdf->addPage($content);
#echo $content;
$pdf->send();
