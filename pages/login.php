<?
$__NO_LOGIN = true;

require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/social.inc.php';

Controller_defaultMethod('loginIndex');

require_once (_file('/controllers/UsuarioCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
        <style>
            .page{
                min-height: 750px;
            }
        </style>
    </head>
    <body>
        <div class="page login-page">
            <div class="container d-flex align-items-center">
                <div class="form-holder has-shadow">
                    <div class="row">
                        <!-- Logo & Information Panel-->
                        <div class="col-lg-6">
                            <div class="info  align-items-center text-center">



                                <? require_once _file('/pages/part/part_capa.php') ?>


                            </div>
                        </div>
                        <!-- Form Panel    -->
                        <div class="col-lg-6 bg-white">
                            <div class="form d-flex align-items-center">
                                <div class="content">

                                    <?if(isset($msgUsuarioValido) && !$msgUsuarioValido):?>
                                        <div class="alert alert-danger" role="alert">


                                            <div>Login e senha não  correspondem</div>

                                        </div>
                                    <?endif?>
                                    <form id="login-form" method="post" class="jquery-validation">


                                        <div class="form-group">
                                             <label id="label_login" for="login" class="label-material">login</label>
                                            <input <?= formInput('login', false, false) ?> type="text"  required="" class="form-control">
                                           



                                        </div>

                                        <div class="form-group">
                                              <label id="label_login" for="senha" class="label-material">Senha</label>
                                            <input <?= formInput('senha', false, false) ?> type="password"  required="" class="form-control">
                                          



                                        </div>

                                        <input type="hidden" value="login" name="go">
                                        <input type="submit" value="Continuar" class="btn btn-primary">

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyrights text-center">


                <? require_once _file('/pages/part/part_login_informacoes.php') ?>

            </div>
        </div>
        <? require_once _file('/pages/part/part_js.php') ?>


    </body>
</html>