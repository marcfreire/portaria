/*global $, document, Chart, LINECHART, data, options, window*/
$(document).ready(function () {

    'use strict';



    // ------------------------------------------------------- //
    // Search Box
    // ------------------------------------------------------ //
    $('#search').on('click', function (e) {
        e.preventDefault();
        $('.search-box').fadeIn();
    });
    $('.dismiss').on('click', function () {
        $('.search-box').fadeOut();
    });

    // ------------------------------------------------------- //
    // Card Close
    // ------------------------------------------------------ //
    $('.card-close a.remove').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.card').fadeOut();
    });


    // ------------------------------------------------------- //
    // Adding fade effect to dropdowns
    // ------------------------------------------------------ //
    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn();
    });
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut();
    });




    // ------------------------------------------------------- //
    // Sidebar Functionality
    // ------------------------------------------------------ //
    $('#toggle-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

        $('.side-navbar').toggleClass('shrinked');
        $('.content-inner').toggleClass('active');

        if ($(window).outerWidth() > 1183) {
            if ($('#toggle-btn').hasClass('active')) {
                $('.navbar-header .brand-small').hide();
                $('.navbar-header .brand-big').show();
            } else {
                $('.navbar-header .brand-small').show();
                $('.navbar-header .brand-big').hide();
            }
        }

        if ($(window).outerWidth() < 1183) {
            $('.navbar-header .brand-small').show();
        }
    });

    // ------------------------------------------------------- //
    // Transition Placeholders
    // ------------------------------------------------------ //
    $('input.input-material').on('focus', function () {
        $(this).siblings('.label-material').addClass('active');
    });

    $('input.input-material').on('blur', function () {
        $(this).siblings('.label-material').removeClass('active');

        if ($(this).val() !== '') {
            $(this).siblings('.label-material').addClass('active');
        } else {
            $(this).siblings('.label-material').removeClass('active');
        }
    });

    // ------------------------------------------------------- //
    // External links to new window
    // ------------------------------------------------------ //
    $('.external').on('click', function (e) {

        e.preventDefault();
        window.open($(this).attr("href"));
    });


    $('.input-group.anexo a').click(function () {

        $(this).closest('.anexo').find('input[type="file"]').trigger("click");

    });
    $('.input-group.anexo input[type="file"]').change(function () {
        var inp = this;
        var names = '';
        for (var i = 0; i < inp.files.length; ++i) {
            names += inp.files.item(i).name + ';';
        }

        var desc = inp.files.length + " arquivo(s) selecionado(s): " + names;
        $(this).closest('.anexo').find('input[type="text"]').val(desc);

    });

    $.extend($.fn.dataTable.defaults, {
        "iDisplayLength": 50,
        "language": {
            "emptyTable": "Não há  registros",
            "infoEmpty": "Não há  registros",
            "search": "Procurar:",
            "info": "Exibindo _START_ de _END_ de _TOTAL_ registros",
            "lengthMenu": "Exibir : _MENU_ registros",
            "zeroRecords": "Nenhum registro localizado",

            "paginate": {
                "next": "Próximo »",
                "previous": "« Anterior",
            }
        }
    });

});
