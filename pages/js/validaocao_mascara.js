$(document).ready(function () {

    'use strict';

    // ------------------------------------------------------- //
    // form validation
    // ------------------------------------------------------ //
    $('.jquery-validation').validate({
        ignore: "",
        invalidHandler: function (form, validator) {
            alertify.alert('Ops!', 'Alguns Campos não foram preenchidos corretamente');
        }
    });

    $.extend(jQuery.validator.messages, {
        required: "Este campo &eacute; requerido.",
        remote: "Por favor, corrija este campo.",
        email: "Por favor, forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.",
        url: "Por favor, forne&ccedil;a uma URL v&aacute;lida.",
        date: "Por favor, forne&ccedil;a uma data v&aacute;lida.",
        dateISO: "Por favor, forne&ccedil;a uma data v&aacute;lida (ISO).",
        number: "Por favor, forne&ccedil;a um n&uacute;mero v&aacute;lido.",
        digits: "Por favor, forne&ccedil;a somente d&iacute;gitos.",
        creditcard: "Por favor, forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
        equalTo: "Por favor, forne&ccedil;a o mesmo valor novamente.",
        accept: "Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
        maxlength: jQuery.validator.format("Por favor, forne&ccedil;a n&atilde;o mais que {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, forne&ccedil;a ao menos {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento."),
        range: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1}."),
        max: jQuery.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
        min: jQuery.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
    });
    $.validator.addClassRules({
        v_req: {
            required: true

        },
        v_min3: {
            minlength: 3
        },
        v_min6: {
            minlength: 6
        },
        v_email: {
            email: true
        },
        v_tel: {
            tel_minlength: 13

        },
        v_date: {
            dateBR: true

        },
        v_time: {
            time: true,
            minlength: 5

        },
        v_day: {
            dayOfMonth: true

        },
        v_cpf: {
            cpf: 'both'

        },
        v_cnpj: {
            cnpj: 'both'

        },
        v_nota: {
            nota: 'both'

        }

    });
    $.validator.addMethod(
            "dateBR",
            function (value, element) {
                var check = false;
                var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                if (re.test(value)) {
                    var adata = value.split('/');
                    var gg = parseInt(adata[0], 10);
                    var mm = parseInt(adata[1], 10);
                    var aaaa = parseInt(adata[2], 10);
                    var xdata = new Date(aaaa, mm - 1, gg);
                    if ((xdata.getFullYear() == aaaa) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == gg))
                        check = true;
                    else
                        check = false;
                } else
                    check = false;
                return this.optional(element) || check;
            },
            "Formato de data inválida"
            );
    $.validator.addMethod(
            "cpf",
            function (value, element) {
                var check = validaCPF(value);
                return this.optional(element) || check;
            },
            "CPF formato inválido"
            );

    // ------------------------------------------------------- //
    // Mask
    // ------------------------------------------------------ //
    $('.masknumber').mask('999999999');
    $('.masktime').mask('99:99');
    $(".maskdate").mask("99/99/9999");
    $(".masktel").mask("(00) 000000000");
    $(".maskcpf").mask("000.000.000-00");
    $(".maskcnpj").mask("00.000.000/0000-00");
    $('.maskmoney').mask('000.000.000.000.000,00', {reverse: true});
    $('.maskcep').mask('00000-000');
    $('.maskrg').mask('000000000000-0');
    $('.masknota').mask('99.99');


});


function validaCPF(cpf) {

    cpf = cpf.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais) {
        numeros = cpf.substring(0, 9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        numeros = cpf.substring(0, 10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    } else
        return false;
}