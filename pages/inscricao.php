<?
require_once __DIR__ . '/../myconfig.php';


require_once __DIR__ . '/social.inc.php';

Controller_defaultMethod('inscricao');

require_once (_file('/controllers/InscricaoCtrl.php'));


?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page form-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
                <style>
                    .hidden{
                        
                        display: none;
                    }
                </style>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
                <!-- Side Navbar -->
                <nav class="side-navbar">
                    <? require_once _file('/pages/part/part_nav.php') ?>
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Portaria Casarão Tech </h2>
                        </div>
                    </header>
                    <ul class="breadcrumb">
                        <div class="container-fluid">
                            <li class="breadcrumb-item"><a href="<?= _src('/') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Formulário</li>
                        </div>
                    </ul>
                    <!-- Forms Section-->
                    <section class="forms"> 
                        <div class="container-fluid">
                            <div class="row">
                                <!-- Basic Form-->
                                <div class="col-lg-12">
                                    
                                    <form  method="POST"  action="<?_src('/pages/inscricao.php')?>" enctype="multipart/form-data" onsubmit="return send()"  class="jquery-validation" id="form1">

                                        <input type="hidden" name="inscricao->idinscricao" value="<?=$_GET['id'] ?? ''?>">
                                        <div class="alert alert-info">
                                            <?if(InscricaoDAO::isUserLogged()):?>
                                                Visitante Já Cadastrado
                                            <?else:?>
                                                Novo Visitante
                                                
                                            <?endif?>
                                        </div>
                                        
                                        <div class="card">

                                            <div class="card-header d-flex align-items-center">
                                                <h3 class="h4"><i class="fa fa-user" aria-hidden="true"></i> Dados Pessoais</h3>
                                            </div>
                                            

                                            <div class="card-body">
                                                
                                                <div class='row'>                                               
                                                
                                                     
                                                     <div class="form-group col-md-12 ">                                                      
                                                           
                                                         <div>
                                                             <label class="form-control-label">Foto*</label>
                                                             <input multiple  type="hidden" id="hidden_data" name="hidden_data"  >
                                                             <div style="background-color: gainsboro;width: 320px;height: 240px;margin-bottom: 10px">
                                                                 
                                                                 
                                                                 <canvas id="snapshot" width=320 height=240    class="<? if (!empty($inscricao->foto)): ?>hidden<?endif?>">
                                                                     
                                                                     
                                                                 </canvas>
                                                                 <? if (!empty($inscricao->foto)): ?>
                                                                     <img id="img-foto" class="mb-5" src="<?= ($inscricao->foto) ?>">
                                                                 <? endif ?>

                                                             </div>
 
                                                             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                                 Capturar
                                                             </button>

                                                             <!-- Modal -->
                                                             <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                 <div class="modal-dialog" role="document">
                                                                     <div class="modal-content">
                                                                         <div class="modal-header">
                                                                             <h5 class="modal-title" id="exampleModalLabel">Foto</h5>
                                                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                 <span aria-hidden="true">&times;</span>
                                                                             </button>
                                                                         </div>
                                                                         <div class="modal-body">
                                                                             <video id="player" style="display: inline-block;width: 100%" controls autoplay></video>

                                                                         </div>
                                                                         <div class="modal-footer">
                                                                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                                             <button id="capture" type="button" class="btn btn-primary" data-dismiss="modal">Capture</button>
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                             </div>

                                                         </div>
                                                    </div>
                                                     
                                                   
                                                    
                                                 
                                            </div>

                                           
                                                <div class="row">
                                                    <div class="form-group col-md-8">
                                                        <label class="form-control-label" for="inscricao_nome">Nome Completo*</label>
                                                        <input required="" type="text" <?= formInput('inscricao->nome') ?>  class="form-control">
                                                    </div>
                                                    
                                                    


                                                    <div class="form-group col-md-4">
                                                        <label class="form-control-label" for="inscricao_cargo">Profissão*</label>
                                                        <input required="" type="text" <?= formInput('inscricao->cargo') ?>  class="form-control">
                                                    </div>



                                                </div>    

                                                <div class="row">

                                       


                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" for="inscricao_nascimento">Data de Nascimento</label>
                                                        <input placeholder="__/__/____"  type="text" <?= formInput('inscricao->nascimento','fixOnlyDate') ?>  class="form-control maskdate v_date">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" for="inscricao_sexo">Sexo*</label>
                                                        <select required=""  <?= formSelect('inscricao->sexo') ?> class="form-control ">
                                                            <option value=""></option>
                                                            <? foreach (ModeloCombo::$boxSexo as $id => $value): ?>
                                                                <option <? @select($id, $inscricao->sexo) ?>  value="<?= $id ?>"><?= $value ?></option>
                                                            <? endforeach ?>
                                                        </select>
                                                    </div>



                                                </div>

                                                  

                                               

                                            </div>
                                        </div>
                                        <div class="card">

                                            <div class="card-header d-flex align-items-center">
                                                <h3 class="h4"><i class="fa fa-map-marker" aria-hidden="true"></i> Endereço</h3>
                                            </div>
                                            <div class="card-body">



                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label class="form-control-label" for="inscricao_endereco">Endereço</label>
                                                            <input  type="text" <?= formInput('inscricao->endereco') ?>  class="form-control">
                                                        </div>
                                                        
                                                         <div class="form-group col-md-6">
                                                        <label class="form-control-label" for="inscricao_complemento">Complemento</label>
                                                        <input  type="text" <?= formInput('inscricao->complemento') ?>  class="form-control ">
                                                    </div>

                                                    </div>    

                                                    <div class="row">

                                                    <div class="form-group col-md-4">
                                                        <label class="form-control-label" for="inscricao_numero">Número</label>
                                                        <input  type="text" <?= formInput('inscricao->numero') ?>  class="form-control">
                                                    </div>



                                                    <div class="form-group col-md-4">
                                                        <label class="form-control-label" for="inscricao_bairro">Bairro</label>
                                                        <input type="text" <?= formInput('inscricao->bairro') ?>  class="form-control ">
                                                    </div>

                                                        
                                                    <div class="form-group col-md-4">
                                                        <label class="form-control-label" for="inscricao_cep">Cep</label>
                                                        <input placeholder="'_____-___"  type="text" <?= formInput('inscricao->cep') ?>  class="form-control maskcep">
                                                    </div>
                                                    



                                                </div>
                                                
                                                <div class="row">
                                                    
                                                   
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" for="inscricao_estado_idestado">Estado</label>
                                                        <select <?= formSelect('inscricao->estado->idestado') ?> class="form-control" data-live-search="true"> 
                                                            <option  value=''>&nbsp</option> 
                                                            <? foreach ($listaEstados as $item) : ?>
                                                                <option <? @select($item->idestado, $inscricao->estado->idestado) ?> value='<?= $item->idestado ?>'><?= $item->uf ?></option> 
                                                            <? endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" for="inscricao_cidade_idcidade">Cidade</label>
                                                        <select <?= formSelect('inscricao->cidade->idcidade') ?> class="form-control " data-live-search="true"> 
                                                            <option  value=''>&nbsp</option>  ?>
                                                        </select>
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                        </div>
                                         
                                         <div class="card">

                                            <div class="card-header d-flex align-items-center">
                                                <h3 class="h4"><i class="fa fa-phone" aria-hidden="true"></i> Contato</h3>
                                            </div>
                                            <div class="card-body">



                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label class="form-control-label" for="inscricao_tel1">Telefone 1*</label>
                                                            <input required="" placeholder="'(__) _________"  type="text" <?= formInput('inscricao->tel1') ?>  class="form-control masktel ">
                                                        </div>
                                                        
                                                        <div class="form-group col-md-6">
                                                            <label class="form-control-label" for="inscricao_tel2">Telefone 2</label>
                                                            <input  placeholder="'(__) _________" type="text" <?= formInput('inscricao->tel2') ?>  class="form-control masktel ">
                                                        </div>
                                                        
                                                        <div class="form-group col-md-6">
                                                            <label class="form-control-label" for="inscricao_email">Email*</label>
                                                            <input required=""  type="text" <?= formInput('inscricao->email') ?>  class="form-control v_email ">
                                                        </div>

                                                    </div>    

                                                    <div class="row">

                                                   



                                                </div>
                                                
                                             

                                            </div>
                                        </div>
                                         
                                        <div class="row">
                                            <div class="form-group col-md-12 text-center pt-5">  
                                                <input type="hidden" name="go" value="inscricaoDO">
                                                <input type="submit" value="Continuar" class="btn btn-primary  btn-lg">
                                                <input  type="hidden" <?= formInput('inscricao->cpf') ?>  class="form-control">
                                            </div>
                                        </div>
                                        
                                       
                                    </form>

                                </div>
                            </div>
                    </section>

                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>
        <script>
            
            
            $(function(){
                
               
                
                $('#inscricao_estado_idestado').change(function () {

                    load_city(false);

                });
                
                
               
                
                $(document).on('click','.excluir-anexo',function(){
                    
                    var tr = $(this).closest('tr');
                    var name = $(this).attr('data-name');
                    alertify.confirm('Tem certeza que deseja excluir o anexo?', function(){
                        
                            excluir(name,function(){
                                
                                tr.fadeOut().remove();
                                
                                
                            });
        		

                    });
                    
                    
                });
                
            });
            
            function send(){
                               
                uploadEx();
               
                                
                if($('#form1').valid()){
                    var $obr = true;
                    <?foreach($anexosObrigatorios as $obr):?>
                    
                        if( $('#<?=$obr?>')[0].files.length > 0 || $('.tr_anexo[data-name="<?=$obr?>"]').length >0 ){

                             
                        }else{
                            alertify.alert('Ops!', "O Anexo '<?=$obr?>' é obrigatório");
                            $obr =  false;
                        }
                    
                    <?endforeach?>
                    
                }
               
                
                
                return $obr;
            }
            
            function load_city(select) {
                
                var fieldAjax = $('#inscricao_cidade_idcidade');
                $.ajax({
                    type: "POST",
                    url: "<?= _url('/pages/ajax/pesquisa-cidades-combo.php') ?>",
                    data: "idestado=" + $('#inscricao_estado_idestado').val(),
                    beforeSend: function () {
                        $(fieldAjax).html("<option value=''>aguarde...</option>");
                       

                    },
                    success: function (txt) {

                        $(fieldAjax).html(txt);
                        
                            
                        if (select) {

                        <? if (!empty($inscricao->cidade->idcidade)): ?>
                                $(fieldAjax).val(<?= $inscricao->cidade->idcidade ?>);
                        <? endif ?>
                        }

                    },
                    error: function (txt) {

                        $(fieldAjax).html("<option value=''>Erro</option>");
                        $(fieldAjax).selectpicker('refresh');
                    }
                });

            }
            
            
            function excluir(name,func){
            
                    $.ajax({
                        type: "POST",
                        url: "<?= _url('/controllers/InscricaoCtrl.php?go=excluirAnexo') ?>",
                        data: "doc=" + name,
                        dataType:'json',
                        beforeSend: function () {

                        },
                        success: function (json) {
                           
                            if(json.ok){
                                func.call(this);
                            }

                        },
                        error: function (txt,e) {

                           alert('Erro');
                         
                        }
                    });
            
            
            }

            load_city(true)
        </script>
        <script>
            var player = document.getElementById('player'); 
            var snapshotCanvas = document.getElementById('snapshot');
            var captureButton = document.getElementById('capture');

            var handleSuccess = function(stream) {
              // Attach the video stream to the video element and autoplay.
              player.srcObject = stream;
            };

            captureButton.addEventListener('click', function() {
              var context = snapshot.getContext('2d');
              // Draw the video frame to the canvas.
              context.drawImage(player, 0, 0, snapshotCanvas.width, 
                  snapshotCanvas.height);
              $('#img-foto').hide();   
              $('#snapshot').show();   
               
            });

            navigator.mediaDevices.getUserMedia({video: true})
                .then(handleSuccess);
        </script>
        <script>
            function uploadEx() {
                var canvas = document.getElementById("snapshot");
                var dataURL = canvas.toDataURL("image/png");
                document.getElementById('hidden_data').value = dataURL;
              
            };
        </script>
        
    </body>
</html>
