<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('setores');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/SetorCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page home-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch">
                <!-- Side Navbar -->
                <nav class="side-navbar">      
                    <? require_once _file('/pages/part/part_nav.php') ?>         
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <!-- Dashboard Counts Section-->        
                    

                    <!-- Projects Section-->
                    <section class="projects">
                        <div class="container-fluid">  

                            <div class="row">
                                <div class="col-md-12 ">  
                                         <a  class="btn btn-success" href="<?=_src('/novo_setor')?>"><i class="fa fa-plus" aria-hidden="true"></i> Nova Sala</a>
                                   
                                </div>
                            </div>

                           
                            <div class="card mt-5">
                                <div class="card-header d-flex align-items-center">
                                    <h3 class="h4"><i class="fa fa-bookmark-o" aria-hidden="true"></i> Salas</h3>
                                </div>
                                <div class="card-body">
                                    <table id="table_id" class="display">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                
                                               <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?foreach($setores as $s):?>
                                            <tr>
                                              
                                                <td><?=$s->nome?></td>
                                             
                                                <td>
                                                    <a href="#." data-id='<?=$s->idsetor?>' class="btn btn-danger btn-sm excluir"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    <a href="<?=_src('novo_setor?id='.$s->idsetor)?>" class="btn btn-info btn-sm "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                                </td>
                                               
                                            </tr>
                                           <?endforeach?>
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </section>




                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>

    </body>
    <script>

        $(document).ready(function () {
           
           
          $('table').DataTable();
          
          $('.excluir').click(function(){
              
               var id = $(this).attr('data-id');
               alertify.confirm('Setor', 'Excluir?', function(){ excluir(id); }
                , function(){});
              
          });
                
         
               
             


           
          });
        
        function excluir(id){
           
            window.location.href = '<?=_src('/setores?go=delete&id=')?>'+id;
           
            
        }
        
    </script>
</html>