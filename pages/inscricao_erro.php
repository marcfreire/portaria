<?
require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/social.inc.php';

Controller_defaultMethod('inscricao');

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page form-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
                <!-- Side Navbar -->
                <nav class="side-navbar">
                    <? require_once _file('/pages/part/part_nav.php') ?>
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech </h2>
                        </div>
                    </header>
                    <ul class="breadcrumb">
                        <div class="container-fluid">
                            <li class="breadcrumb-item"><a href="<?= _src('/') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Erro</li>
                        </div>
                    </ul>
                    <!-- Forms Section-->
                    <section class="forms"> 
                        <div class="container-fluid">
                            <div class="row">
                                <!-- Basic Form-->
                                <div class="col-lg-12">
                                    
                                    <div class="alert alert-danger" role="alert">
                                                
                                              Houve um erro na sua inscrição, Por favor contate o suporte técnico do Iema e  informe a mensagem :
                                              <?=$_GET['erro']?>
                                   </div>

                                </div>
                            </div>
                            <div class="row">
                                <!-- Basic Form-->
                                <div class="col-lg-12 text-center">
                                    
                                    <a href="<?= _src('/inscricao')?>" class="btn btn-lg btn-info">Tentar novamente</a>

                                </div>
                            </div>
                    </section>

                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>
        <script>
            
            
            
       
            
           
        </script>
    </body>
</html>