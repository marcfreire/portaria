<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('visitas');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page home-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch">
                <!-- Side Navbar -->
                <nav class="side-navbar">      
                    <? require_once _file('/pages/part/part_nav.php') ?>         
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <!-- Dashboard Counts Section-->        

                    <!-- Projects Section-->
                    <section class="projects">
                        <div class="container-fluid">  

                            

                           
                            <div class="card">
                                <div class="card-header d-flex align-items-center">
                                    <h3 class="h4"><i class="fa fa-list" aria-hidden="true"></i> Visitas Registradas</h3>
                                </div>
                                <div class="card-body">
                                    <table id="table_id" class="display">
                                        <thead>
                                            <tr>
                                                <th>Data e Hora</th>
                                                 <th>Nome</th>
                                                <th>Setor</th>
                                                 <th>Orgão</th>
                                                 <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?foreach($visitas as $v):?>
                                            <tr>
                                                <td><?= fixDate($v->datacadastro)?></td>
                                                <td><a href="<?=_src('')?>"><?=$v->visita_nome?></a></td>
                                                <td><?=$v->setor_nome?></td>
                                                <td><?=$v->setor_mestre_nome?></td>
                                                <td>
                                                    <a class="btn btn-success" href="<?=_src('/visita_detalhes?cod=').$v->idvisita?>">  <i class="fa fa-info" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn btn-danger excluir" data-id="<?=$v->idvisita ?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                           <?endforeach?>
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </section>




                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>

    </body>
    <script>

        $(document).ready(function () {
            $('#table_id').DataTable();

            $('.excluir').click(function(){              
              var id = $(this).attr('data-id');

              alertify
                .confirm(
                    'Visita',
                    'Excluir?',
                    function(){ excluir(id); },
                    function(){}
                )
             
            });

            function excluir(id){
                window.location.href = '<?=_src('/visitas?go=delete&id=')?>'+id        
            }
        });
    </script>
</html>
