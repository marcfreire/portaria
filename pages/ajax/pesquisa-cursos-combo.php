<?php

// Para solicionar problema de ACENTOS
#header('Content-Type: text/html; charset=ISO-8859-1');

require_once __DIR__ . '/../../myconfig.php';
require_once (_file('/lib/myphp/ConnectFactory.class.php'));
require_once (_file('/dao/CidadeDAO.class.php'));

$idUnidade = $_POST["idunidade"];



try {
    
    $pdo = ConnectFactory::getConnect();

    $stmt = $pdo->prepare("
            select c.* from vaga as v 
            

            inner join curso as c on v.idcurso = c.idcurso
            

            where v.stdelete = 1 and v.idunidade = :idunidade and selecao_ano = 2018
            
            
            
        ");

    $stmt->bindParam(':idunidade', $idUnidade, PDO::PARAM_INT);

    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_OBJ);

    if ($result) {

        echo " <option value=\"\">Selecione</option>";
        foreach ($result as $item) {

            echo "<option value=\"$item->idcurso\">$item->nome</option>";
        }
    } else {

        echo " <option value=\"\">Selecione</option>";
    }
} catch (PDOException $i) {

    echo "Erro: <code>" . $i->getMessage() . "</code>";
}



