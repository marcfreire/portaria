<?php

// Para solicionar problema de ACENTOS
#header('Content-Type: text/html; charset=ISO-8859-1');

require_once __DIR__.'/../../myconfig.php';
require_once (_file('/lib/myphp/ConnectFactory.class.php'));
require_once (_file('/dao/CidadeDAO.class.php'));

$idEstado = $_POST["idestado"];

$cidadeDao = new CidadeDAO();
$result = $cidadeDao->pesquisaPorEstado($idEstado);

if ($result) {

    echo " <option value=\"\">Selecione</option>";
    foreach ($result as $item) {

        echo "<option value=\"$item->idcidade\">$item->nome</option>";
    }
} else {
    
   echo " <option value=\"\">Selecione</option>";
}
