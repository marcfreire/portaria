<?php

if (!UsuarioDAO::isUserLogged() && !isset($__NO_LOGIN)) {

    header('Location:' . _src('/login'));
}