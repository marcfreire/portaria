<?
$__NO_LOGIN = true;

require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/social.inc.php';



Controller_defaultMethod('registro');

require_once (_file('/controllers/InscricaoCtrl.php'));

?>
<!DOCTYPE html>
<html>
  <head>
     <? require_once _file('/pages/part/part_head.php') ?>
      <style>
          .page{
              min-height: 750px;
          }
      </style>
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info  align-items-center text-center">
                <? require_once _file('/pages/part/part_capa.php') ?>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                     <?if(isset($mensagemErro[0])):?>
                        <div class="alert alert-danger" role="alert">

                                <?foreach($mensagemErro as $erro):?>                        
                                 <div><?=$erro?></div>
                                <?endforeach?>

                        </div>
                     <?endif?>
                    
                    <form id="register-form" class="jquery-validation" method="post">
                    <div class="form-group">
                      <input <?= formInput('inscricao->cpf',false,false) ?> type="text"  required class="input-material maskcpf v_cpf">
                      <label for="inscricao_cpf" class="label-material">CPF</label>
                    </div>
                    <div class="form-group">
                      <input <?= formInput('inscricao->email',false,false) ?> type="email"  required class="input-material v_email">
                      <label for="inscricao_email" class="label-material">Endereço de Email</label>
                    </div>
                    <div class="form-group">
                      <input <?= formInput('inscricao->senha',false,false) ?> type="password"  required class="input-material">
                      <label for="inscricao_senha"  class="label-material">Senha</label>
                    </div>
                      
                     <div class="form-group">
                      <input id="senha2" type="password" name="senha2" required class="input-material">
                      <label for="senha2" class="label-material">Cofirme sua Senha</label>
                    </div>
                      
                     <input type="hidden" value="registroDO" name="go">
                    <input id="register" type="submit" value="Registrar" class="btn btn-primary">
                  </form><small>Já possui cadastro? </small><a href="<?=_src('/login')?>" class="signup">Login</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
           <? require_once _file('/pages/part/part_login_informacoes.php') ?>
      </div>
    </div>
      <? require_once _file('/pages/part/part_js.php') ?>
      <script>
          
          $(function(){
              
                $('#senha2').rules("add",
                {
                   equalTo: "#inscricao_senha",
                   messages: { equalTo: 'Senha não correspondente.' }
                    
                });             
              
              
          });
          
           
      </script>
  </body>
</html>