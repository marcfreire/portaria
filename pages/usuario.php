<?
require_once __DIR__ . '/../myconfig.php';


require_once __DIR__ . '/social.inc.php';

Controller_defaultMethod('index');

require_once (_file('/controllers/UsuarioCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
<? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page form-page">
            <!-- Main Navbar-->
            <header class="header">
<? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
                <!-- Side Navbar -->
                <nav class="side-navbar">
<? require_once _file('/pages/part/part_nav.php') ?>
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech </h2>
                        </div>
                    </header>
                    <ul class="breadcrumb">
                        <div class="container-fluid">
                            <li class="breadcrumb-item"><a href="<?= _src('/usuarios') ?>">Usúarios</a></li>
                            <li class="breadcrumb-item active"><a href="<?= _src('/usuario') ?>">Novo Usuários</a></li>
                        </div>
                    </ul>
                    <!-- Forms Section-->
                    <section class="forms"> 
                        <div class="container-fluid">
                            <div class="row">
                                <!-- Basic Form-->
                                <div class="col-lg-12">

                                    <form  method="POST"  action="<? _src('/pages/inscricao.php') ?>" enctype="multipart/form-data" onsubmit="return send()"  class="jquery-validation" id="form1">







                                        <div class="card">

                                            <div class="card-header d-flex align-items-center">
                                                <h3 class="h4"><i class="fa fa-user" aria-hidden="true"></i> Usuário</h3>
                                            </div>
                                            <div class="card-body">



                                                <div class="row">


                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" >Nome*</label>
                                                        <input required placeholder=""  type="text" <?= formInput('usuario->nome') ?>  class="form-control  ">
                                                    </div>





                                                </div>  
                                                <div class="row">


                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" >Email(login)*</label>
                                                        <input required placeholder=""  type="text" <?= formInput('usuario->email') ?>  class="form-control  ">
                                                    </div>


                                                </div>


                                                <div class="row">


                                                    <div class="form-group col-md-6">
                                                        <label class="form-control-label" >Foto</label>
                                                        <input id="foto" name="foto" required placeholder=""  type="file"   class="form-control  ">
                                                    </div>

                                                </div>    


                                                <div class="row">


                                                <? if (empty($usuario->senha)): ?>
                                                        <div class="form-group col-md-6">
                                                            <label class="form-control-label" >Senha*</label>
                                                            <input required placeholder=""  type="password" <?= formInput('usuario->senha') ?>  class="form-control  ">
                                                        </div>
                                                <? endif ?>


                                                </div>




                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-12 text-center pt-5">  
                                                <input type="hidden" name="go" value="create">
                                                <input type="submit" value="Salvar" class="btn btn-primary  btn-lg">
                                                <input  type="hidden" <?= formInput('usuario->idusuario') ?>  class="form-control">
                                            </div>
                                        </div>


                                    </form>

                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
<? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>
        <script>
            $(function () {

                $('.js-example-basic-single').select2({

                    theme: "classic"
                });
            });
        </script>

    </body>
</html>