<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('vista_detalhes');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page home-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch">
                <!-- Side Navbar -->
                <nav class="side-navbar">      
                    <? require_once _file('/pages/part/part_nav.php') ?>         
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <!-- Dashboard Counts Section-->
                    
                    

                    <!-- Projects Section-->
                    <section class="projects">
                        <div class="container-fluid">  
                            
                            <a class="btn btn-secondary mb-2" href="javascript:history.back()"><i class="fa fa-angle-left" aria-hidden="true"></i>
 Voltar</a>

                            <div class="card-header d-flex align-items-center">
                                <h3 class="h4"><i class="fa  fa-info" aria-hidden="true"></i> Detalhes</h3>
                            </div>

                            <div class="card ">
                                <div class="card-body">
                                    <? if (!empty($inscricao->foto)): ?>
                                    <img class="mb-5" src="<?= ($inscricao->foto) ?>">
                                    <? endif ?>
                                    <div><strong>Visitante: </strong><?= $visita->visita_nome ?></div>
                                    <div><strong>Data e Hora: </strong><?= fixDate($visita->datacadastro) ?></div>
                                    <div><strong>Orgão: </strong><?= ($visita->setor_mestre_nome) ?></div>
                                    <div><strong>Setor: </strong><?= ($visita->setor_nome) ?></div>
                                   
                                    <div><strong>Nome Instituição: </strong><?= fixDate($visita->instituicao) ?></div>
                                    <div><strong>Tipo de Instituição: </strong><?= fixDate($visita->tipo_instituicao) ?></div>

                                </div>

                            </div>



                        </div>
                    </section>




                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>

    </body>
    <script>

        $(document).ready(function () {
            $('#table_id').DataTable();
        });
    </script>
</html>