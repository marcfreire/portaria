<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('nova_visita');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/InscricaoCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page home-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch">
                <!-- Side Navbar -->
                <nav class="side-navbar">      
                    <? require_once _file('/pages/part/part_nav.php') ?>         
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <!-- Dashboard Counts Section-->        

                    <!-- Projects Section-->
                    <section class="projects">
                        <form  method="POST"  action="<? _src('/pages/inscricao.php') ?>" enctype="multipart/form-data" onsubmit="return send()"  class="jquery-validation" id="form1">


                            <div class="container-fluid">  



                                <div class="card">

                                    <div class="card-header d-flex align-items-center">
                                        <h3 class="h4"><i class="fa fa-plus-square" aria-hidden="true"></i> Nova Visita</h3>
                                    </div>


                                    <div class="card-body">



                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-control-label" for="inscricao_cpf">CPF/RG</label>
                                                <input  type="text" <?= formInput('inscricao->cpf',false,false) ?>  class="form-control maskcpf">
                                            </div>


                                            <!--

                                            <div class="form-group col-md-6">
                                                <label class="form-control-label" for="inscricao_rg">Rg</label>
                                                <input  type="text" <?= formInput('inscricao->rg',false,false) ?>  class="form-control ">
                                            </div>
                                            -->



                                        </div>    







                                    </div>

                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12 text-center pt-5">  
                                        <input type="hidden" name="go" value="registro">
                                        <input type="submit" value="Continuar" class="btn btn-primary  btn-lg">
                                    </div>
                                </div>



                            </div>
                        </form>
                    </section>




                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>

    </body>
    <script>

        $(document).ready(function () {
            $('#table_id').DataTable();
        });
    </script>
</html>