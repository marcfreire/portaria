<?
require_once __DIR__ . '/../myconfig.php';

Controller_defaultMethod('alteraSenha');

require_once __DIR__ . '/social.inc.php';

require_once (_file('/controllers/UsuarioCtrl.php'));
?>
<!DOCTYPE html>
<html>
    <head>
        <? require_once _file('/pages/part/part_head.php') ?>
    </head>
    <body>
        <div class="page form-page">
            <!-- Main Navbar-->
            <header class="header">
                <? require_once _file('/pages/part/part_header.php') ?>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
                <!-- Side Navbar -->
                <nav class="side-navbar">
                    <? require_once _file('/pages/part/part_nav.php') ?>
                </nav>
                <div class="content-inner">
                    <!-- Page Header-->
                    <header class="page-header">
                        <div class="container-fluid">
                            <h2 class="no-margin-bottom">Casarão Tech</h2>
                        </div>
                    </header>
                    <ul class="breadcrumb">
                        <div class="container-fluid">
                            <li class="breadcrumb-item"><a href="<?= _src('/') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Alterar Senna</li>
                        </div>
                    </ul>
                    <!-- Forms Section-->
                    <section class="forms"> 
                        <div class="container-fluid">
                            <div class="row">
                                <!-- Basic Form-->
                                <div class="col-lg-12">
                                    
                                    <form  method="POST"  action="" enctype="multipart/form-data"  class="jquery-validation" id="form1">

                                        
                                         <?if(isset($sucesso) && $sucesso):?>
                                            <div class="alert alert-success" role="alert">
                                                
                                               Senha Alterada com Sucesso! 
                                            </div>
                                        
                                         <?endif?>
                                        
                                         <?if(isset($force) && $force && !isset($sucesso)):?>
                                            <div class="alert alert-warning" role="alert">
                                                
                                               Por motivos de segurança, é necessário você alterar sua senha! 
                                            </div>
                                        
                                         <?endif?>
                                        
                                        <div class="card">

                                            <div class="card-header d-flex align-items-center">
                                                <h3 class="h4"><i class="fa fa-key" aria-hidden="true"></i> Alterar Senha</h3>
                                            </div>
                                            
                                            
                                            <div class="card-body">



                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label class="form-control-label" for="inscricao_senha">Nova senha*</label>
                                                        <input required="" type="password" <?= formInput('usuario->senha',false,false) ?>  class="form-control">
                                                    </div>


                                                    <div class="form-group col-sm-6">
                                                        <label class="form-control-label" for="senha2">Confirme sua senha*</label>
                                                        <input required="" type="password" id="senha2" name="senha2" class="form-control">
                                                    </div>
                                                    

                                                </div>    

                                            </div>
                                        </div>
                                                                              
                                    
                                        <div class="row">
                                            <div class="form-group col-md-12 text-center pt-5">  
                                                <input type="hidden" name="go" value="alterarSenha">
                                                <input type="submit" value="Alterar senha" class="btn btn-primary btn-lg">
                                            </div>
                                        </div>
                                        
                                    </form>

                                </div>
                            </div>
                    </section>

                </div>
            </div>
        </div>
        <? require_once _file('/pages/part/part_footer.php') ?>
        <? require_once _file('/pages/part/part_js.php') ?>
        <script>
            
            
            $(function(){
                
                $('#senha2').rules("add",
                {
                   equalTo: "#usuario_senha",
                   messages: { equalTo: 'Senha não correspondente.' }
                    
                });  
                
            });
            
       
            
           
        </script>
    </body>
</html>