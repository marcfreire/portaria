<?
$__NO_LOGIN = true;

require_once __DIR__ . '/../myconfig.php';

require_once __DIR__ . '/social.inc.php';

Controller_defaultMethod('registro');

require_once (_file('/controllers/InscricaoCtrl.php'));

?>
<!DOCTYPE html>
<html>
  <head>
      <? require_once _file('/pages/part/part_head.php') ?>
      <style>
          .page{
              min-height: 750px;
          }
      </style>
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info  align-items-center text-center">
                
                
                    
                     <? require_once _file('/pages/part/part_capa.php') ?>
                     
                
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                    
                    <?if(isset($mensagemErro[0])):?>
                        <div class="alert alert-danger" role="alert">

                                <?foreach($mensagemErro as $erro):?>                        
                                 <div><?=$erro?></div>
                                <?endforeach?>

                        </div>
                     <?endif?>
                    
                     <?if(isset($usuarioValido) && $usuarioValido):?>
                        <div class="alert alert-success" role="alert">
                           Enviamos um e-mail para <?=$inscricao->email?> com as instruções necessárias para recuperação de senha.

                        </div>
                     <?endif?>
                    
                    <h3>Recuperar Senha</h3>
                    <form id="login-form" method="post" class="jquery-validation pt-5">
                    <div class="form-group">
                      <input <?= formInput('inscricao->cpf',false,false) ?> type="text"  required="" class="maskcpf input-material">
                      <label for="inscricao_cpf" class="label-material">Login(CPF)</label>
                      
                      
                      
                    </div>
                    
                        <input type="hidden" value="recuperarSenha" name="go">
                      <input type="submit" value="Continuar" class="btn btn-primary">
                  
                  </form>
                    <a href="<?=_src('/login')?>" class="forgot-pass">Voltar para o login</a><br>
                    
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
          
           <? require_once _file('/pages/part/part_login_informacoes.php') ?>
        

      </div>
    </div>
      <? require_once _file('/pages/part/part_js.php') ?>
      
      <script>
      
      $(function(){
          
          
        
         
          
      });
          
      </script>
    
  </body>
</html>